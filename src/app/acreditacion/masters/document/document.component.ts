import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MatSnackBar, MatPaginatorIntl } from '@angular/material';
import { MasterService } from 'app/acreditacion/services/master.service';
import { DialogDeleteComponent } from 'app/acreditacion/dialog-delete/dialog-delete.component';
import { DocumentFormComponent } from './document-form/document-form.component';
import { Document } from './../../models/document'
import { Router } from '@angular/router';

@Component({
  selector: 'ms-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit {

  @ViewChild(MatPaginatorIntl) paginator: MatPaginatorIntl;

  @ViewChild(MatSort) sort: MatSort

  isLoad: boolean = false
  displayedColumns: string[] = ['id',  'id_usertype', 'name', 'status','actions'];
  dataSource: any
  constructor(private masterService: MasterService,
    private dialog: MatDialog, private snackBar: MatSnackBar,private router:Router
  ) { }

  ngOnInit() {
    this.loadDocuments()
  }
  applyFilter(filterValue: String) {
    this.dataSource.filter = filterValue.trim().toLowerCase()
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage()
    }
  }
  private loadDocuments() {

    this.isLoad = true
    this.masterService.getDocuments().subscribe((data: any) => {
      /* this.nts = data
      this.nts.sort(function (obj1, obj2) {
        return obj2.id - obj1.id
      }) */
      console.log(data)
      this.dataSource = new MatTableDataSource(data)
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.isLoad = false
    },
      error => {
        this.openSnackBar(error.name, 'Error')
        this.isLoad = false
      })
  }
  onSetDocument(data: Document) {
    this.isLoad = true
    this.masterService.addDocument(data).subscribe(() => {
      this.loadDocuments()
      this.isLoad = false
      this.openSnackBar("Se añadio correctamente", "ÉXITO")
    }, error => {
      this.isLoad = false
      this.openSnackBar(error.name, 'Error')

    })
  }
  onEditDocument(data: Document) {
    this.isLoad = true

    this.masterService.updateDocument(data).subscribe(() => {
      this.loadDocuments()
      this.openSnackBar("Se editó corectamente", "ÉXITO")

    }, error => {
      this.isLoad = false
      this.openSnackBar(error.name, 'Error')

    })
  }
  onUpdateStatus(data: Document) {

    let status: string = (~~(!parseInt(data.status))).toString()
    let Document1: Document = new Document(data.id,data.name,data.path, data.id_user,status)
    this.onEditDocument(Document1)
  }
  onDeleteDocument(id: any) {
    this.isLoad = true
    this.masterService.deleteDocument(id).subscribe(() => {
      this.loadDocuments()
      this.isLoad = false
      this.openSnackBar("Se eliminó correctamente", "ÉXITO")
    }, error => {
      this.openSnackBar(error.name, 'Error')
      this.isLoad = false

    })
  }
  openForm(data:any){
    this.router.navigate(['master/document/'+data])
  }

/*   openModal(data: any) {
    let rpv1: Document
    if (data) {
      rpv1 = new Document(data.id,data.codigo, data.titulo,data.direccion,data.autor,data.estado)
    } else {
      rpv1 = null
    }

    const dialogRef = this.dialog.open(DocumentFormComponent, {
      data: rpv1,
      width: '500px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result == 'Close') {
        console.log('close')
      } else {
        if (result.id == null) {
          this.onSetDocument(result)
        } else {
          this.onEditDocument(result)
        }
      }
    })
  } */
  // pregunta de eliminar
  onDelete(i: any) {
    const dialogRef = this.dialog.open(DialogDeleteComponent,{data:"¿Estás seguro de que quieres eliminar este Documento de forma permanente?", width:'450px'})

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        this.onDeleteDocument(i.id)
      }
    })
  } 

  //snackBar
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      verticalPosition: 'top'
    });
  }
}
