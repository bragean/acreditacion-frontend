import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MatSnackBar, MatPaginatorIntl } from '@angular/material';
import { MasterService } from 'app/acreditacion/services/master.service';
import { DialogDeleteComponent } from 'app/acreditacion/dialog-delete/dialog-delete.component';
import { User } from 'app/acreditacion/models/user';
import { FormUserComponent } from './form-user/form-user.component';
@Component({
  selector: 'ms-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  @ViewChild(MatPaginatorIntl) paginator: MatPaginatorIntl;

  @ViewChild(MatSort) sort: MatSort

  isLoad: boolean = false
  displayedColumns: string[] = ['id','username', 'first_name', 'user_type', 'status', 'action'];
  dataSource: any
  constructor(private masterService: MasterService,
    private dialog: MatDialog, private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.loadUsers()
  }
  applyFilter(filterValue: String) {
    this.dataSource.filter = filterValue.trim().toLowerCase()
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage()
    }
  }
  private loadUsers() {

    this.isLoad = true
    this.masterService.getUsers().subscribe((data: any) => {
      /* this.nts = data
      this.nts.sort(function (obj1, obj2) {
        return obj2.id - obj1.id
      }) */
      console.log(data)
      this.dataSource = new MatTableDataSource(data)
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.isLoad = false
    },
      error => {
        this.openSnackBar(error.name, 'Error')
        this.isLoad = false
      })
  }
  onSetUser(data: User) {
    this.isLoad = true
    this.masterService.addUser(data).subscribe(() => {
      this.loadUsers()
      this.isLoad = false
      this.openSnackBar("Se añadio correctamente", "ÉXITO")
    }, error => {
      this.isLoad = false
      this.openSnackBar(error.name, 'Error')

    })
  }
  onEditUser(data: User) {
    this.isLoad = true

    this.masterService.updateUser(data).subscribe(() => {
      this.loadUsers()
      this.openSnackBar("Se editó corectamente", "ÉXITO")

    }, error => {
      this.isLoad = false
      this.openSnackBar(error.name, 'Error')

    })
  }
  onUpdateStatus(data: User) {

 /*    let status: string = (~~(!parseInt(data.estado))).toString()
    let Document1: User = new User(data.id,data.codigo,data.titulo,data.direccion,data.autor,status)
    this.onEditUser({
      
    }) */
  }
  onDeleteUser(id: any) {
    this.isLoad = true
    this.masterService.deleteUser(id).subscribe(() => {
      this.loadUsers()
      this.isLoad = false
      this.openSnackBar("Se eliminó correctamente", "ÉXITO")
    }, error => {
      this.openSnackBar(error.name, 'Error')
      this.isLoad = false

    })
  }

  openModal(data: any) {


    const dialogRef = this.dialog.open(FormUserComponent, {
      data: data,
      width: '500px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result == 'Close') {
        console.log('close')
      } else {
        if (result.id == null) {
          this.onSetUser(result)
        } else {
          this.onEditUser(result)
        }
      }
    })
  }
  // pregunta de eliminar
  onDelete(i: any) {
    const dialogRef = this.dialog.open(DialogDeleteComponent,{data:"¿Estás seguro de que quieres eliminar este Documento de forma permanente?", width:'450px'})

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        this.onDeleteUser(i.id)
      }
    })
  } 

  //snackBar
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      verticalPosition: 'top'
    });
  }
}

