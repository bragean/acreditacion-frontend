import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestartLoginComponent } from './restart-login.component';

describe('RestartLoginComponent', () => {
  let component: RestartLoginComponent;
  let fixture: ComponentFixture<RestartLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestartLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestartLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
