import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskProccessComponent } from './task-proccess.component';

describe('TaskProccessComponent', () => {
  let component: TaskProccessComponent;
  let fixture: ComponentFixture<TaskProccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskProccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskProccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
