import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Injectable } from '@angular/core';
import { RestartLoginComponent } from 'app/session/loginV2/restart-login/restart-login.component';
import {  MatDialog } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor(
    public router: Router,
    private toastr: ToastrService,
    private dialog: MatDialog) { }

  public getError(error:any){
    //this.toastr.error(error.statusText,'ERROR')

    switch(error.status){
      case 401:
          const dialogRef = this.dialog.open(RestartLoginComponent, { 
            disableClose:true,       
            width: '500px'
          })      
        //this.router.navigate(['/session/loginV2'])
        break;
      case 500:
        this.toastr.error('Intente de nuevo','ERROR')
        break;
      case 404:
        //this.toastr.error('Página no encontrada','ERROR')
        this.router.navigate(['/evaluaciones'])
        break;
      default:
        //this.toastr.error(error.status,'ERROR')
        this.router.navigate(['/evaluaciones'])
        return;
    }
  }

}
