import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FormPhaseComponent } from '../../process/form-phase/form-phase.component';
import { MasterService } from 'app/acreditacion/services/master.service';
import {ErrorService} from 'app/acreditacion/services/error.service';
@Component({
  selector: 'ms-form-competence',
  templateUrl: './form-competence.component.html',
  styleUrls: ['./form-competence.component.scss']
})
export class FormCompetenceComponent implements OnInit {

  public form: FormGroup
  public id: any = ''
  public users: any[] = []
  indicators: any[] = []
  criterias: any[] = []
  constructor(private fb: FormBuilder, 
    public dialogRef: MatDialogRef<FormPhaseComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private masterService: MasterService,
    private error:ErrorService) {
    this.form = this.fb.group({
      id: null,
      id_indicator: null,
      name: null,
      end_date: new Date(),
      id_criteria: null,
      user_list: null,
      description: null,
    })
  }

  ngOnInit() {
    this.initForm()
    this.getUsers()
    this.loadCriterias()
    //this.getIndicators()
  }
  getUsers() {

    this.masterService.getUsersRoleByProccess(this.masterService.idprocess).subscribe((data: any) => {
      console.log(data)

      this.users = data
    }, error => {
      this.error.getError(error)
    })
  }
  getIndicators(data) {
    this.masterService.getIndicatorsByCriteria(data).subscribe((data: any) => {
      console.log(data)
      this.indicators = data
    },error=>{
      this.error.getError(error)
    })
  }

  onCancel(): void {
    this.dialogRef.close('Cancel')
  }

  initForm() {
    if (this.data == null) {
      this.id = null
    }
    else {
      console.log("actualizare", this.data)
      this.id = this.data.id
      this.data.user_list = this.data.user_list.split(',')

      this.form.setValue(
        {
          id: this.data.id,
          id_indicator: this.data.id_indicator,
          name: this.data.name,
          end_date: new Date(this.data.end_date),
          id_criteria: this.data.id_criteria,
          user_list: this.data.user_list,
          description: this.data.description,
        }

      )
      this.form.get('id_criteria').setValue(parseInt(this.data.id_criteria))
      this.form.get('id_indicator').setValue(parseInt(this.data.id_indicator))
      this.getIndicators(this.data.id_criteria)
    }
  }

  selectIndicatorByCriterio(data) {
    this.getIndicators(data)
  }

  onSubmit(data: any) {
    if (this.form.valid) {

      console.log(this.form.value)

      this.dialogRef.close({
        id: this.form.value.id,
        id_indicator: this.form.value.id_indicator,
        id_criteria: this.form.value.id_criteria,
        name: this.form.value.name,
        end_date: this.form.value.end_date.toISOString().split('T')[0],

        /* status_task: this.form.value.status_task, */
        user_list: this.form.value.user_list.toString(),
        description: this.form.value.description,
        /*  status: this.form.value.status, */
        /*  close_date: this.form.value.close_date, */
        /*   status_delivery: this.form.value.status_delivery,
          status_review: this.form.value.status_review, */
      });
      this.form.reset()
    }
  }
  onClose() {
    this.dialogRef.close('Close')
    this.form.reset()
  }

  loadCriterias() {
    this.masterService.getCriteriasByProcess(this.masterService.idprocess).subscribe((data: any) => {
      console.log(data)
      this.criterias = data

    },
      error => {
        this.error.getError(error)
      })
  }

}

