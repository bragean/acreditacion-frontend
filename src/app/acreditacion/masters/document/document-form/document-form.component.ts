import { Document } from './../../../models/document';
import { MasterService } from './../../../services/master.service';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'ms-document-form',
  templateUrl: './document-form.component.html',
  styleUrls: ['./document-form.component.scss']
})
export class DocumentFormComponent implements OnInit {

  public form: FormGroup
  public id: any = ''
  document: Document

  types: any[]
  constructor(private router: Router, private navigate: ActivatedRoute, private masterService: MasterService, private fb: FormBuilder, private snackbar: MatSnackBar) {
    this.form = this.fb.group({
      id: null,
      name: ['', [Validators.required]],
      path: ['', [Validators.required]],
      id_user: ['', [Validators.required]],
      status: '1'
    })
  }

  ngOnInit() {
    this.initForm()
  }
  onGetDocumentById(id) {
    this.masterService.getDocument(id).subscribe((data: any) => {
      console.log(data)
      this.document = data
      this.form.setValue({
        id:data.id,
        name:data.name,
        path:data.path,
        id_user: data.id_user,
        status: data.status
      })
    }, erro => {
      alert(erro.name)
    })
  }

  onClose(): void {
    this.form.reset()
    this.router.navigate(["master/document"])
  }

  initForm() {
    let param = this.navigate.snapshot.params["id"]

    if (param == "null") {
      this.id = null
      this.document = new Document()
    }
    else {
      console.log(param)
      this.id = param
      this.onGetDocumentById(this.id)
    }
  }

  onSubmit(data: any) {
    console.log(data)
    if (this.id == null) {
      this.onSetDocument(this.form.value)
    } else {
      this.onEditDocument(data)
    }
  }
  onSetDocument(data: Document) {
    this.masterService.addDocument(data).subscribe(() => {
      this.openSnackBar("Se añadio correctamente", "ÉXITO")
    }, error => {
      this.openSnackBar(error.name, 'Error')

    })
  }
  onEditDocument(data: Document) {

    this.masterService.updateDocument(data).subscribe(() => {

      this.openSnackBar("Se editó corectamente", "ÉXITO")

    }, error => {
      this.openSnackBar(error.name, 'Error')

    })
  }
  onFileChange(event, data: string) {
    console.log(event)
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.form.get(data).setValue(file);
    }
  }
  //snackBar
  openSnackBar(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 3000,
      verticalPosition: 'top'
    });
  }

}