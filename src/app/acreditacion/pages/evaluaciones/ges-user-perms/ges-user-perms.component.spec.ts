import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GesUserPermsComponent } from './ges-user-perms.component';

describe('GesUserPermsComponent', () => {
  let component: GesUserPermsComponent;
  let fixture: ComponentFixture<GesUserPermsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GesUserPermsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GesUserPermsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
