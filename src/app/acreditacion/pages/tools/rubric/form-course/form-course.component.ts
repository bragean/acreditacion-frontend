import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'ms-form-course',
  templateUrl: './form-course.component.html',
  styleUrls: ['./form-course.component.scss']
})
export class FormCourseComponent implements OnInit {

  public form: FormGroup
  public id:any =''
  list_type: any[] = ["Básico","De Formación","Avanzado","Otros"]

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<FormCourseComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.form = this.fb.group({
      id: null,
      name: null,
      description: null,
      status: '1'
    })
  }

  ngOnInit() {
    this.initForm()
  }

  onCancel(): void {
    this.dialogRef.close('Cancel')
  }

  initForm() {
    if (this.data == null) {
      this.id=null
      
    }
    else {
      console.log("actualizare",this.data)
      this.id=this.data.id
      this.form.setValue({
        id: this.data.id,
        name: this.data.name,
        status:this.data.status
      })
    }
  }

  onSubmit(data:any) {
    if(this.form.valid){
      console.log(data)
      this.dialogRef.close(data)
      this.form.reset()
    }
  }
  onClose(){
    this.dialogRef.close('Close')
    this.form.reset()
  }

}