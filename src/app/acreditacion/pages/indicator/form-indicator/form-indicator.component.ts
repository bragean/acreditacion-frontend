import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FormPhaseComponent } from '../../process/form-phase/form-phase.component';
import { MasterService } from 'app/acreditacion/services/master.service';
import {ErrorService} from 'app/acreditacion/services/error.service';

@Component({
  selector: 'ms-form-indicator',
  templateUrl: './form-indicator.component.html',
  styleUrls: ['./form-indicator.component.scss']
})
export class FormIndicatorComponent implements OnInit {

  public form: FormGroup
  public id: any = ''
  list_criteria: any[] = []

  constructor(private fb: FormBuilder, 
    public dialogRef: MatDialogRef<FormPhaseComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: any,
    private masterService: MasterService,
    private error:ErrorService) {
    this.form = this.fb.group({
      id: null,
      id_criteria: null,
      name: null,
      description: null,
      status: true
    })
  }

  ngOnInit() {
    this.initForm()

  }

  onCancel(): void {
    this.dialogRef.close('Cancel')
  }

  initForm() {
    if (this.data == null) {
      this.id = null

    }
    else {
      this.id = this.data.id
      this.form.setValue(
        {
          id: this.data.id,
          id_criteria: this.data.id_criteria,
          name: this.data.name,
          description: this.data.description,
          status: this.data.status
        }

      )
    }
    this.loadCriterias(this.masterService.process.id)
  }

  onSubmit(data: any) {
    if (this.form.valid) {
      this.dialogRef.close(this.form.value)
      this.form.reset()
    }
  }
  private loadCriterias(id) {
    this.masterService.getCriteriasByProcess(id).subscribe((data: any) => {
      data.forEach(element => {
        this.list_criteria.push({
          id: element.id,
          name: element.name
        })
      });
    },
      error => {
        this.error.getError(error)
      })
  }
  onClose() {
    this.dialogRef.close('Close')
    this.form.reset()
  }

}
