import { Component, OnInit } from '@angular/core';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { MasterService } from 'app/acreditacion/services/master.service';
import { AuthService } from 'app/acreditacion/services/auth.service';
import { ErrorService } from 'app/acreditacion/services/error.service';

declare var jsPDF:any;

@Component({
  selector: 'ms-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  process: any = []
  user: any
  id_process: any = ''
  name_process: any=''
  id_phase: ''
  comboPhase: any[] = []

  taskList: any[] = []

  isLoad: boolean = false

  constructor(private pageTitleService: PageTitleService,
    private masterService: MasterService,
    private auth: AuthService,
    private error: ErrorService,) { }

  ngOnInit() {
    this.user = this.auth.getLSuser()
    this.pageTitleService.setTitle("Reportes");
    this.loadProcesssGeneral()
  }

  private loadProcesssGeneral() {

    this.isLoad = true
    this.masterService.getProcesss().subscribe((data: any) => {
      this.process=data
      this.isLoad = false
    },
      error => {
        this.isLoad = false
        this.error.getError(error)
      })
  }

  getPhasesByProcess(id) {
    this.process.forEach(element => {
      if(element.id==id){
        this.name_process=element.name;
      }
    });

    this.masterService.getPhasesByProcess(id).subscribe((data: any) => {
      this.comboPhase = data
    },error=>{
      this.error.getError(error)
    })
  }

  getTaskByphase() {
    this.masterService.getTaskBYphaseAndIdUser(this.id_phase, 'owner').subscribe((data: any) => {
      if (Array.isArray(data)) {
        this.taskList = data
        this.getReport(data)       
      } else {      
        alert(data.status)
      }
    },error=>{
      this.error.getError(error)
    })
  }

  public loadTasks() {
    this.masterService.getTaskByPhase(this.id_phase).subscribe((data: any) => {

      this.taskList = data
      this.getReport(data)
    },
      error => {
        this.isLoad = false
        this.error.getError(error)
      })
  }


  public getReport(data:any[]){

    const doc=new jsPDF();
    doc.text(20,20,'Universidad Nacional de San Agustín');
    doc.setFontSize(14)
    doc.text(20,30,'Escuela Profesional de Ingenieria de sistemas');
    doc.setFontSize(10)
    doc.text(20,40,'Usuario: '+this.user.username)
    doc.text(20,50,'Proceso: '+this.name_process)
    console.log(data)

    
    var cuerpo=[];
    var col=["Tarea","Descripcion","Responsable","Fecha de entrega"];
    data.forEach(element => {
      var temp=[element.name,element.description,element.user_list,element.end_date.split("T",1)];
      cuerpo.push(temp);
    });
    //método para mandar a imprimir el documento
    //doc.autoPrint();
    doc.autoTable(col,cuerpo,{margin:{top:60}});
    doc.save('report.pdf');
  }

}
