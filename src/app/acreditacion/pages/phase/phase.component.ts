import { MasterService } from './../../services/master.service';
import { Component, OnInit } from '@angular/core';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ms-phase',
  templateUrl: './phase.component.html',
  styleUrls: ['./phase.component.scss']
})
export class PhaseComponent implements OnInit {
  
  id_phase = ''
  name_phase=''

  constructor(public ar : ActivatedRoute, private masterData: MasterService, 
    public pageTitleService: PageTitleService,
    private route: Router,) {

    

    if(this.masterData.phase!=null){
      this.id_phase = this.masterData.phase.id;
      this.name_phase = this.masterData.phase.name;

      this.masterData.idphases=this.id_phase
    }else{
      this.route.navigate(['evaluaciones'])
    }

    
  }
  ngOnInit() {
    this.pageTitleService.setTitle(this.name_phase)
  }
}
