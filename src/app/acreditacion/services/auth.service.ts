import { map } from 'rxjs/operators';
import { Injectable,NgZone } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomainService } from 'app/acreditacion/services/domain.service';
import { Observable } from 'rxjs';
import { isNullOrUndefined } from 'util';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private htttp: HttpClient, 
    private dom: DomainService,
    private toastr: ToastrService,
    public router: Router,
    private zone: NgZone) 
    { }

  headers: HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
    Authorization: 'Bearer ' + this.getToken()
  });

  
  getToken() {
    return localStorage.getItem("accessToken");
  }

  registerUser(user: any) {
    const url_api = this.dom.getDomain() + "register/";
    return this.htttp
       .post(
          url_api,
          user
       )
       .pipe(map(data => data));
 }

  loginuser(username: string, password: string): Observable<any> {
    const url_api = this.dom.getDomain() + "auth/login/";
    return this.htttp
      .post(
        url_api,
        { username, password }
      )
      .pipe(map(data => data));
  }

  
  setToken(token): void {
    localStorage.setItem("accessToken", token);
  }

  getUserServiceByToken(token) {
    let headers: HttpHeaders = new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: 'Bearer ' + token
    });
    const url_api = this.dom.getDomain() + "api/user/get/select/";
    return this.htttp.get(url_api, { headers: headers })
  }

  setLSuser(data: any) {
    localStorage.setItem("user", JSON.stringify(data));
  }

  getLSuser() {
    let user_string = localStorage.getItem("user");
    if (!isNullOrUndefined(user_string)) {
      let user = JSON.parse(user_string);
      return user;
    } else {
      return null;
    }
  }

  logOut() {

    localStorage.removeItem("user");
    localStorage.removeItem("accessToken");
    localStorage.removeItem("currentUser");  

    this.toastr.success("Cierre de sesión correcta!");
    this.router.navigate(['/session/loginV2']);
    
    this.zone.runOutsideAngular(() => {
       location.reload();
    });
 }

//para arriba revisado

  setUser(user: any): void {
    let user_string = JSON.stringify(user);
    localStorage.setItem("currentUser", user_string);
  }

  getCurrentUser() {
    let user_string = localStorage.getItem("user");
    if (!isNullOrUndefined(user_string)) {
      let user = JSON.parse(user_string);

      return user;
    } else {
      return null;
    }
  }

  logoutUser() {
    let accessToken = localStorage.getItem("accessToken");
    let headers2: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + accessToken
    })
    const url_api = this.dom.getDomain() + "api/main/mobility/logout/"
    localStorage.removeItem("accessToken");
    localStorage.removeItem("currentUser");
    localStorage.removeItem("user");
    return this.htttp.get(url_api, { headers: headers2 });
  }

  getPermission() {
    let array: Array<any> = this.getCurrentUser().permissions
    return array;
  }
  getAuth(modulo: string, perm: any): boolean {
    if (this.getCurrentUser().is_superuser == 'True') {
      return true
    } else {
      let acc = this.getPermission().find(el => {
        return el.name == modulo
      })
      console.log(acc.result.split(',')[perm - 1])
      return acc.result.split(',')[perm - 1] == '1'
    }
  }

  //validar para la descarga 
  getUserValidate(username, password) {
    const url_api = this.dom.getDomain() + "api/main/mobility/get/user/"
    return this.htttp
      .post(
        url_api,
        { username, password },
        { headers: this.headers }
      )
      .pipe(map(data => data));
  }
}