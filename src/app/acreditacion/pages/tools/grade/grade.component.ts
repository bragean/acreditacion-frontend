import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MatSnackBar, MatPaginatorIntl } from '@angular/material';
import { MasterService } from 'app/acreditacion/services/master.service';
import { DialogDeleteComponent } from 'app/acreditacion/dialog-delete/dialog-delete.component';
import { User } from 'app/acreditacion/models/user';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'ms-grade',
  templateUrl: './grade.component.html',
  styleUrls: ['./grade.component.scss']
})
export class GradeComponent implements OnInit {

  @ViewChild(MatPaginatorIntl) paginator: MatPaginatorIntl;

  @ViewChild(MatSort) sort: MatSort

  fields: string[] = ['file'];
  public form: FormGroup;



  constructor(
    private fb: FormBuilder,
    private pageTitleService: PageTitleService,
    private masterService: MasterService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
) { 
    this.form = this.fb.group({
        file: ['']
    })
}


  ngOnInit() {
    this.pageTitleService.setTitle("Notas");
  }
  
  onSubmit(data: any) {
    console.log(data)
    this.sendGrades(data)
}   

sendGrades(data: any) {
  let formData = new FormData()
  formData.append('file', data.file)
  console.log("holaa")
  /*this.masterService.sendGrades(data).subscribe(() => {
      //this.loadUser()
      this.openSnackBar("Se editó corectamente", "ÉXITO")
  }, error => {
      this.openSnackBar(error.name, 'Error')
  })*/
}

openSnackBar(message: string, action: string) {
  this.snackBar.open(message, action, {
    duration: 3000,
    verticalPosition: 'top'
  });
}

public onArchivoSeleccionado($event) {
  if ($event.target.files.length === 1) {
    this.form.get('file').setValue($event.target.files[0])
  }
}



}

