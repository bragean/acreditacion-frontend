import { ListTaskComponent } from './acreditacion/pages/taskManagement/list-task/list-task.component';
import { ControlComponent } from './acreditacion/pages/control/control.component';
import { UserComponent } from './acreditacion/configuration/user/user.component';
import { UserTypeComponent } from './acreditacion/configuration/user-type/user-type.component';
import { DocumentComponent } from './acreditacion/masters/document/document.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HorizontalLayoutComponent } from './horizontal-layout/horizontal-layout.component';
import { MainComponent } from './main/main.component';
import { CommingsoonComponent } from './pages/commingsoon/commingsoon.component';
import { MaintenanceComponent } from './pages/maintenance/maintenance.component';
import { AuthGuard } from './core/guards/auth.guard';
import { EvaluacionesComponent } from './acreditacion/pages/evaluaciones/evaluaciones.component';
import { UserProfileComponent } from './acreditacion/pages/user-profile/user-profile.component';
import { DocumentFormComponent } from './acreditacion/masters/document/document-form/document-form.component';
import { PhaseComponent } from './acreditacion/pages/phase/phase.component';
import { ProcessComponent } from './acreditacion/pages/process/process.component';
import { ReportComponent } from './acreditacion/pages/report/report.component';
import { TaskProccessComponent } from './acreditacion/pages/taskManagement/task-proccess/task-proccess.component';
import { GradeComponent } from './acreditacion/pages/tools/grade/grade.component';
import { RubricComponent } from './acreditacion/pages/tools/rubric/rubric.component';
import { CourseComponent } from './acreditacion/pages/course/course.component';
const appRoutes: Routes = [
   {
      path: '',
      redirectTo: 'control',
      pathMatch: 'full',
   },
   {
      path: 'session',
      loadChildren: './session/session.module#SessionModule',
   },
   {
      path: 'pages/comingsoon',
      component: CommingsoonComponent
   },
   {
      path: 'pages/maintenance',
      component: MaintenanceComponent
   },
   {
      path: '',
      component: MainComponent,
      canActivate: [AuthGuard],
      runGuardsAndResolvers: 'always',
      children: [
         { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
         { path: 'inbox', loadChildren: './inbox/inbox.module#InboxModule' },
         { path: 'chat', loadChildren: './chat/chat.module#ChatModule' },
         { path: 'calendar', loadChildren: './calendar/calendar.module#Calendar_Module' },
         { path: 'editor', loadChildren: './editor/editor.module#EditorModule' },
         { path: 'icons', loadChildren: './material-icons/material-icons.module#MaterialIconsModule' },
         { path: 'chart', loadChildren: './chart/chart.module#ChartModule' },
         { path: 'components', loadChildren: './components/components.module#ComponentsModule' },
         { path: 'dragndrop', loadChildren: './drag-drop/drag-drop.module#DragDropModule' },
         { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
         { path: 'forms', loadChildren: './forms/forms.module#FormModule' },
         { path: 'maps', loadChildren: './maps/maps.module#MapsModule' },
         { path: 'pages', loadChildren: './pages/pages.module#PagesModule' },
         { path: 'users', loadChildren: './users/users.module#UsersModule' },
         { path: 'ecommerce', loadChildren: './ecommerce/ecommerce.module#EcommerceModule' },
         { path: 'video-player', loadChildren: './video-player/video-player.module#VideoPlayerModule' },
         { path: 'taskboard', loadChildren: './task-board/task-board.module#TaskBoardModule' },
         { path: 'courses', loadChildren: './courses/courses.module#CoursesModule' },
         { path: 'user-management', loadChildren: './user-management/user-management.module#UserManagementModule' },
         { path: 'crypto', loadChildren: './crypto/crypto.module#CryptoModule' },
         { path: 'crm', loadChildren: './crm/crm.module#CrmModule' },
         { path: 'master/document', component: DocumentComponent },
         { path: 'master/document/:id', component: DocumentFormComponent },
         { path: 'evaluaciones', component: EvaluacionesComponent },
         { path: 'process', component: ProcessComponent },
         { path: 'phase', component: PhaseComponent },
         { path: 'configuration/usertype', component: UserTypeComponent },
         { path: 'configuration/user', component: UserComponent },
         { path: 'control', component: ControlComponent },
         { path: 'report', component: ReportComponent },
         { path: 'task-list', component: ListTaskComponent },
         { path: 'task-process/:id/:option', component: TaskProccessComponent },
         { path: 'task-process', component: TaskProccessComponent },
         { path: 'profile', component: UserProfileComponent },
         { path: 'tools/grade', component: GradeComponent },
         { path: 'tools/rubric', component: RubricComponent },
         { path: 'course/:id', component: CourseComponent },
      ]
   }
]

@NgModule({
   imports: [RouterModule.forRoot(appRoutes)],
   exports: [RouterModule],
   providers: []
})
export class RoutingModule { }
