import { Component, OnInit, ViewChild, Input,NgZone } from '@angular/core';
import { DialogDeleteComponent } from 'app/acreditacion/dialog-delete/dialog-delete.component';
import { FormCriteriaComponent } from './form-criteria/form-criteria.component';
import { MatTableDataSource, MatSnackBar, MatDialog, MatPaginatorIntl, MatSort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterService } from 'app/acreditacion/services/master.service';
import { AuthService } from 'app/acreditacion/services/auth.service';
import {ErrorService} from 'app/acreditacion/services/error.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'ms-criteria',
  templateUrl: './criteria.component.html',
  styleUrls: ['./criteria.component.scss']
})
export class CriteriaComponent implements OnInit {
  @Input() id_processPadre: string

  @ViewChild(MatPaginatorIntl) paginator: MatPaginatorIntl;

  @ViewChild(MatSort) sort: MatSort

  isLoad: boolean = false
  displayedColumns: string[] = ['name', 'description', 'create', 'action'];
  dataSource: any

  id_process = ''

  user

  constructor(private masterService: MasterService,
    private dialog: MatDialog, private snackBar: MatSnackBar,
    private route: Router, private ar: ActivatedRoute,
    private authService: AuthService,
    private error:ErrorService,
    private toastr: ToastrService,
    private zone: NgZone
  ) { }

  ngOnInit() {
    this.configurePage();
  }

  public configurePage(){
    if(this.masterService.process!=null){
      this.id_process = this.masterService.process.id;
      this.loadCriterias(this.id_process);
      this.user = this.authService.getLSuser()
    }  
  }

  openCriteria(data) {
    this.route.navigate(['criteria/' + data.id])
  }

  applyFilter(filterValue: String) {
    this.dataSource.filter = filterValue.trim().toLowerCase()
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage()
    }
  }

  private loadCriterias(id) {
    this.isLoad = true
    this.masterService.getCriteriasByProcess(id).subscribe((data: any) => {
      this.dataSource = new MatTableDataSource(data)
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.isLoad = false
    },
      error => {
        this.isLoad = false
        this.error.getError(error)
      })
  }

  onSetCriteria(data: any) {
    this.isLoad = true
    this.masterService.addCriteria({
      //id: data.id,
      name: data.name,
      id_process: Number(this.id_process),
      finished: data.finished,
      description: data.description,
      //status: data.status
    }).subscribe((data) => {
      this.loadCriterias(this.id_process)
      this.isLoad = false
      this.toastr.success('Se añadió correctamente','ÉXITO')
    }, error => {
      this.isLoad = false
      this.error.getError(error)
    })
  }

  onEditCriteria(data: any) {
    this.isLoad = true

    this.masterService.updateCriteria(data).subscribe(() => {
      this.loadCriterias(this.id_process)
      this.toastr.success('Se editó corectamente','ÉXITO')

    }, error => {
      this.isLoad = false
      this.error.getError(error)
    })
  }
  onUpdateStatus(data: any) {

    /* let status: string = (~~(!parseInt(data.status))).toString()
    let Criteria1: Criteria = new Criteria(data.id,data.name, status)
    this.onEditCriteria(Criteria1) */
  }

  onDeleteCriteria(id: any) {
    this.isLoad = true
    this.masterService.deleteCriteria(id).subscribe(() => {
      this.loadCriterias(this.id_process)
      this.isLoad = false
      this.toastr.success('Se eliminó correctamente','ÉXITO')
    }, error => {
      this.isLoad = false
      this.error.getError(error)
    })
  }

  openModal(data: any) {

    const dialogRef = this.dialog.open(FormCriteriaComponent, {
      data: data,
      width: '500px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result == 'Close') {
      } else {
        if (result.id == null) {
          this.onSetCriteria(result)
        } else {
          this.onEditCriteria(result)
        }
      }
    })
  }

  // pregunta de eliminar
  onDelete(i: any) {
    const dialogRef = this.dialog.open(DialogDeleteComponent, { data: "¿Estás seguro de que quieres eliminar este criterio de forma permanente? se eliminarán los indicadores y tareas asociados", width: '450px' })

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        this.onDeleteCriteria(i.id)
      }
    })
  }

  //snackBar
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      verticalPosition: 'top'
    });
  }

  public checkRole(){
    return this.masterService.rol;
  }
}