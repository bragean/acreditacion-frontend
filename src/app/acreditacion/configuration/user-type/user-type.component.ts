import { FormUserTypeComponent } from './form-user-type/form-user-type.component';
import { UserType } from './../../models/usertype';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatDialog, MatSnackBar, MatTableDataSource, MatPaginatorIntl } from '@angular/material';
import { MasterService } from 'app/acreditacion/services/master.service';
import { DialogDeleteComponent } from 'app/acreditacion/dialog-delete/dialog-delete.component';

@Component({
  selector: 'ms-user-type',
  templateUrl: './user-type.component.html',
  styleUrls: ['./user-type.component.scss']
})
export class UserTypeComponent implements OnInit {


  @ViewChild(MatPaginatorIntl) paginator: MatPaginatorIntl;

  @ViewChild(MatSort) sort: MatSort

  isLoad: boolean = false


  displayedColumns: string[] = ['id', 'name','status', 'action'];
  dataSource: any


  constructor(private masterService: MasterService,
    private dialog: MatDialog, private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.loadUserTypes()
  }
  applyFilter(filterValue: String) {
    this.dataSource.filter = filterValue.trim().toLowerCase()
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage()
    }
  }
  private loadUserTypes() {

    this.isLoad = true
    this.masterService.getUserTypes().subscribe((data: any) => {
      /* this.nts = data
      this.nts.sort(function (obj1, obj2) {
        return obj2.id - obj1.id
      }) */
      console.log(data)
      this.dataSource = new MatTableDataSource(data)
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.isLoad = false
    },
      error => {
        this.openSnackBar(error.name, 'Error')
        this.isLoad = false
      })
  }
  onSetUserType(data: UserType) {
    this.isLoad = true
    this.masterService.addUserType(data).subscribe(() => {
      this.loadUserTypes()
      this.isLoad = false
      this.openSnackBar("Se añadio correctamente", "ÉXITO")
    }, error => {
      this.isLoad = false
      this.openSnackBar(error.name, 'Error')

    })
  }
  onEditUserType(data: any) {
    this.isLoad = true

    this.masterService.updateUserType(data).subscribe(() => {
      this.loadUserTypes()
      this.openSnackBar("Se editó corectamente", "ÉXITO")

    }, error => {
      this.isLoad = false
      this.openSnackBar(error.name, 'Error')

    })
  }
  onUpdateStatus(data: UserType) {

    let status: string = (~~(!parseInt(data.status))).toString()
    let UserType1: UserType = new UserType(data.id,data.name, status)
    this.onEditUserType(UserType1)
  }
  onDeleteUserType(id: any) {
    this.isLoad = true
    this.masterService.deleteUserType(id).subscribe(() => {
      this.loadUserTypes()
      this.isLoad = false
      this.openSnackBar("Se eliminó correctamente", "ÉXITO")
    }, error => {
      this.openSnackBar(error.name, 'Error')
      this.isLoad = false

    })
  }

  openModal(data: any) {
    let rpv1: UserType
    if (data) {
      rpv1 = new UserType(data.id,data.name,data.status)
    } else {
      rpv1 = null
    }

    const dialogRef = this.dialog.open(FormUserTypeComponent, {
      data: rpv1,
      width: '500px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result == 'Close') {
        console.log('close')
      } else {
        if (result.id == null) {
          this.onSetUserType(result)
        } else {
          this.onEditUserType(result)
        }
      }
    })
  }
  // pregunta de eliminar
  onDelete(i: any) {
    const dialogRef = this.dialog.open(DialogDeleteComponent,{data:"¿Estás seguro de que quieres eliminar este Tipo de Usuario de forma permanente?", width:'450px'})

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        this.onDeleteUserType(i.id)
      }
    })
  } 

  //snackBar
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      verticalPosition: 'top'
    });
  }
}