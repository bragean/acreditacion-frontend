import { AuthService } from 'app/acreditacion/services//auth.service';
import { MasterService } from './../../../services/master.service';
import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { ErrorService } from 'app/acreditacion/services/error.service';

@Component({
  selector: 'ms-ges-user-perms',
  templateUrl: './ges-user-perms.component.html',
  styleUrls: ['./ges-user-perms.component.scss']
})
export class GesUserPermsComponent implements OnInit {

  public form: FormGroup
  public id: any = ''
  public users: any[] = []
  public user: any

  searchUser = ''
  listUsers: any[] = [];

  listRoles: any[] = []
  rolSelected = ''

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<GesUserPermsComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
    private toastr: ToastrService,
    private masterService: MasterService, 
    private auth: AuthService,
    private error:ErrorService,) {
    this.user = this.auth.getLSuser().id

    this.form = this.fb.group({
      id: null,
      name: null,
      id_user: this.user,
      user_list: [[]],
      documents_list: '',
      description: null,
      status: true,
    })
  }

  ngOnInit() {
    this.ongetRol()
    this.getUsers()
  }

  getUsers() {
    this.masterService.getUsersRoleByProccess(this.data.id).subscribe((data: any) => {

      data.forEach(element => {
        element.image = 'assets/img/user-11.jpg'
      });
      this.listUsers = data
    }, error => {
      this.error.getError(error)
    })
  }
  
  getId() {
    return this.user.toString()
  }

  onCancel(): void {
    this.dialogRef.close('Cancel')
  }

  initForm() {

  }

  onSubmit(data: any) {

    if (this.form.valid) {

      this.dialogRef.close({
        id: this.form.value.id,
        name: this.form.value.name,
        id_user: this.form.value.id_user,
        user_list: this.form.value.user_list.toString(),
        documents_list: this.form.value.documents_list,
        description: this.form.value.description,
        status: this.form.value.status
      })
      this.form.reset()
    }
  }
  onClose() {
    this.dialogRef.close('Close')
    this.form.reset()
  }


  onsearchUser() {
    let aux: any[] = []
    this.listUsers.forEach(element => {
      aux.push(element.username)
    });
    if (aux.includes(this.searchUser)) {
      this.toastr.error('Este usuario ya existe','ERROR')
      return
    }
    let us = {
      username: this.searchUser
    }
    this.masterService.searchUser(us).subscribe((res: any) => {
      if (res.response == "succes") {

        let send = {
          id_process: this.data.id,
          username: this.searchUser,
        }
        this.masterService.setUserByProcess(send).subscribe((data: any) => {
          this.getUsers()
        }, error => {
          this.error.getError(error)
        })
      } else {
        this.toastr.error('Usuario no encontrado','ERROR')
      }
    },
    error=>{
      this.error.getError(error)
    })
  }

  onSetRolByUser(item) {
    let send = {
      id_process: this.data.id,
      username: item.username,
      code: item.code
    }
    this.masterService.setRolByUserByProcess(send).subscribe((data: any) => {
      this.getUsers()
    }, error => {
      this.error.getError(error)
    })
  }

  deleteUser(item) {
    let send = {
      id_process: this.data.id,
      username: item.username,

    }

    this.masterService.deleteuserByProccess(send).subscribe((data: any) => {
      this.getUsers()
    }, error => {
      this.error.getError(error)
    })
  }

  ongetRol() {
    this.masterService.getRoles().subscribe((data: any) => {
      this.listRoles = data

    }, error => {
      this.error.getError(error)
    })
  }

}