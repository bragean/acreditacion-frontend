import { MasterService } from 'app/acreditacion/services/master.service';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { AuthService } from 'app/acreditacion/services/auth.service';
import { ErrorService } from 'app/acreditacion/services/error.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'ms-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.scss']
})
export class ControlComponent implements OnInit {

  process: any[] = []
  fases: any[] = []
  tasks: any[] = []

  id_process: number
  id_phase:any

  user: any

  processData: any = {
    owner:"",
    created: '',
    description: '',
    documents_list: '',
    id: '',
    last_updated: '',
    name: '',
    name_user: "",
    status: "",
    user_list: "",
    status_process: '0'
  }

  projectStatus: any[] = []
  projectStatusPersonal: any[] = []

  //sale chart data 
  saleChartData: any[] = []

  phaseSelected:any={name:''};
  statusPhase:any=0;
  view:any


  constructor(private pageTitleService: PageTitleService,
    private masterData: MasterService,
    private auth: AuthService,
    private cd: ChangeDetectorRef,
    private error:ErrorService,
    private toastr: ToastrService,) { }

  ngOnInit() {
    this.configPage();
    this.pageTitleService.setTitle("Tablero de control");
  }

  public configPage(){
    this.user = this.auth.getLSuser();
    this.loadProcesssGeneral();
  }

  loadProcesssGeneral() {
    this.masterData.getProcesss().subscribe((data: any) => {
      if (data === undefined) {
        return
      }
      this.process = data
    },
      error => {
        console.log(error)
        this.error.getError(error)
      })
  }

  userName: any[] = []
  names: string = ""

  loadUsers(id) {
    this.masterData.getUsersByProcess(id).subscribe((data: any) => {
      if (!data)
        return;

      this.userName = data

      this.userName.forEach(element => {
        this.names = this.names + element.first_name + ", "
      });

      this.process[0].user_list = this.names
    },
      error => {
        this.error.getError(error)
      })
  }

  loadPhases(idprocess) {
    let pro = this.process.find(element => {
      return element.id == this.id_process ? element : null;
    });

    this.processData = pro

    this.masterData.getPhasesByProcess(idprocess).subscribe((data: any) => {
      if (!data)
        return;

      console.log(data)

      this.fases = data; 

      this.saleChartData = []
      this.fases.forEach(element => {
        this.saleChartData.push({
          id: element.id,
          title: element.name,
          value: parseInt(element.status_phase)
        })
      });
      this.cd.detectChanges()
    },
      error => {
        this.error.getError(error)
      })
  }

  loadTask(idfase) {

    this.getPhase(idfase);

    this.masterData.getTaskByPhase(idfase).subscribe((data: any) => {
      this.tasks = data
      this.getProgressOfPhase(data);
      this.projectStatusPersonal = []
      this.projectStatus = []
      this.tasks.forEach(element => {
        var listUsers:any[]=[]

        if(element.user_list!=null){
          listUsers=element.user_list.split(",")
        }

        let color = element.status_task == '0' ? 'warn-bg' : (element.status_task == '1' ? 'primary-bg' : 'success-bg')
        this.projectStatus.push({
          id: element.id,
          title: element.name,
          color: color,
          duration: element.status_task == '0' ? 'Sin iniciar' : (element.status_task == '1' ? 'En proceso' : (element.status_task == '2' ? 'Completado' : 'Revisado' )),
          value: element.status_task == '0' ? 0 : (element.status_task == '1' ? 50 : 100)
        })

        listUsers.forEach(ele=>{
          if(ele==this.user.username){
            this.projectStatusPersonal.push({
              id: element.id,
              title: element.name,
              color: color,
              duration:element.status_task == '0' ? 'Sin iniciar' : (element.status_task == '1' ? 'En proceso' : (element.status_task == '2' ? 'Completado' : 'Revisado' )),
              value: element.status_task == '0' ? 0 : (element.status_task == '1' ? 50 : 100)
            })
          }
        })
             
      });
      this.cd.detectChanges()
    },
      error => {
        this.error.getError(error)
      })
  }

  parse(val: string) {
    return parseFloat(val)
  }

  public sort(list:any[]){
    
  }

  public getPhase(idphase){
    this.fases.forEach(element => {
      if(idphase==element.id){
        this.phaseSelected=element;
      }
    });
  }

  public getProgressOfPhase(tasks:any[]){
    if(tasks.length==0){
      this.statusPhase=0;
    }else{
      var progress=0;
      this.tasks.forEach(element => {
        if(element.status_task==3){
          progress+=1;
        }
      });   
      this.statusPhase=Math.round((progress/tasks.length)*100);
    } 
  }

  public changeView(view:any){
    this.view=view;
  }

  public validateView(){
    if(this,this.view=="personal"){
      return false;
    }else{
      return true;
    }
  }

}