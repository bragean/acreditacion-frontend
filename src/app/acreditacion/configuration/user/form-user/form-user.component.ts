import { MasterService } from './../../../services/master.service';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'ms-form-user',
  templateUrl: './form-user.component.html',
  styleUrls: ['./form-user.component.scss']
})
export class FormUserComponent implements OnInit {

  public form: FormGroup
  public id: any = ''
  usertypes: any[] = []

  types: any[]
  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<FormUserComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private masterService: MasterService) {
    this.form = this.fb.group({
      id: null,
      first_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      username: ['', [Validators.required]],
      password: null,
      status: true
    })
  }

  ngOnInit() {
    this.initForm()
    this.getUserType()
  }

  onCancel(): void {
    this.dialogRef.close('Cancel')
  }

  initForm() {
    if (this.data == null) {
      this.id = null

    }
    else {
      console.log("actualizare", this.data)
      this.id = this.data.id
      this.form.setValue({
        id: this.data.id,
        first_name: this.data.first_name,
        last_name: this.data.last_name,
        username: this.data.username,
        password: null,
        status: this.data.status
      })
    }
  }

  onSubmit(data: any) {
    if (this.form.valid) {
      console.log(data)
      this.dialogRef.close({
        id: data.id,
        first_name: data.first_name,
        last_name: data.last_name,
        username: data.username,
        password: data.password,
        status: data.status
      })
      this.form.reset()
    }
  }
  onClose() {
    this.dialogRef.close('Close')
    this.form.reset()
  }
  getUserType() {
    /* this.masterService.getUserTypes().subscribe((data: any) => {
      console.log(data)
      this.usertypes = data
    }, err => {
      console.log(err)
    }) */
    this.usertypes = [
      "Administrador",
      "Usuario"
    ]
  }

}