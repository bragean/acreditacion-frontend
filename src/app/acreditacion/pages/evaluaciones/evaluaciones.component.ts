import { Router } from '@angular/router';
import { ErrorService } from 'app/acreditacion/services/error.service';
import { PermisosService } from 'app/acreditacion/services/permisos.service';

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginatorIntl, MatSort, MatDialog, MatSnackBar, MatTableDataSource } from '@angular/material';
import { MasterService } from 'app/acreditacion/services/master.service';
import { DialogDeleteComponent } from 'app/acreditacion/dialog-delete/dialog-delete.component';
import { FormProcessComponent } from './form-process/form-process.component';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { GesUserPermsComponent } from './ges-user-perms/ges-user-perms.component';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'app/acreditacion/services/auth.service';


@Component({
  selector: 'ms-evaluaciones',
  templateUrl: './evaluaciones.component.html',
  styleUrls: ['./evaluaciones.component.scss']
})
export class EvaluacionesComponent implements OnInit {


  @ViewChild(MatPaginatorIntl) paginator: MatPaginatorIntl;

  @ViewChild(MatSort) sort: MatSort

  isLoad: boolean = false


  displayedColumns: string[] = ['name', 'id_user', 'description', 'action'];
  dataSource: any
  user: any
  listUsers: any[] = [];

  constructor(public authService: AuthService,
    private masterService: MasterService,
    private dialog: MatDialog, private snackBar: MatSnackBar,
    private route: Router, public pageTitleService: PageTitleService,
    private error: ErrorService,
    private permiso:PermisosService,
    private toastr: ToastrService
  ) {
    this.pageTitleService.setTitle("Procesos")
  }

  ngOnInit() {
    this.loadProcesssGeneral()
  }

  public checkRole(data:any) {
    return this.permiso.validateUserByProcess(data);
  }

  openProcess(data) {
    this.masterService.process=data
    this.route.navigate(['process'])
  }

  applyFilter(filterValue: String) {
    this.dataSource.filter = filterValue.trim().toLowerCase()
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage()
    }
  }
  private loadProcesssGeneral() {

    this.isLoad = true
    this.masterService.getProcesss().subscribe((data: any) => {
      this.dataSource = new MatTableDataSource(data)
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.isLoad = false
    },
      error => {
        this.isLoad = false
        this.error.getError(error)
      })
  }

  onSetProcess(data: any) {
    this.isLoad = true
    this.masterService.addProcess(data).subscribe(() => {
      this.loadProcesssGeneral()
      this.isLoad = false
      this.toastr.success('Se añadió correctamente', 'ÉXITO')
    }, error => {
      this.isLoad = false
      this.error.getError(error)
    })
  }

  onEditProcess(data: any) {
    this.isLoad = true
    this.masterService.updateProcess(data).subscribe(() => {
      this.loadProcesssGeneral()
      this.toastr.success('Se editó correctamente', 'ÉXITO')
    }, error => {
      this.isLoad = false
      this.error.getError(error)
    })
  }

  onDeleteProcess(id: any) {
    this.isLoad = true
    this.masterService.deleteProcess(id).subscribe(() => {
      this.loadProcesssGeneral()
      this.isLoad = false
      this.toastr.success('Se eliminó correctamente', 'ÉXITO')
    }, error => {
      this.isLoad = false
      this.error.getError(error)
    })
  }

  openModal(data: any) {
    let rpv1: any
    if (data) {
      rpv1 = {
        id: data.id,
        name: data.name,
        id_user: data.id_user,
        user_list: data.user_list,
        documents_list: data.documents_list,
        description: data.description,
        status: data.status
      }
    } else {
      rpv1 = null
    }

    const dialogRef = this.dialog.open(FormProcessComponent, {
      data: rpv1,
      width: '500px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result == 'Close') {
      } else {
        if (result.id == null) {
          this.onSetProcess(result)
        } else {

          let send = {
            id: result.id,
            name: result.name,
            documents_list: result.documents_list,
            description: result.description,
            status: result.status
          }
          this.onEditProcess(send)
        }
      }
    })
  }

  openGesUsers(data: any) {
    const dialogRef = this.dialog.open(GesUserPermsComponent, {
      data: data,
      width: '800px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result == 'Close') {
      } else {
      }
      this.loadProcesssGeneral()
    })
  }

  // pregunta de eliminar
  onDelete(i: any) {
    const dialogRef = this.dialog.open(DialogDeleteComponent, { data: "¿Estás seguro de que quieres eliminar este proceso de forma permanente? Se eliminarán los criterios, indicadores, fases y tareas asociados", width: '450px' })

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        this.onDeleteProcess(i.id)
      }
    })
  }

  //snackBar
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      verticalPosition: 'top'
    });
  }
  
}