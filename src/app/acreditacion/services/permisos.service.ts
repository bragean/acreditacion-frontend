import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Injectable } from '@angular/core';
import { MasterService } from 'app/acreditacion/services/master.service';
import { ErrorService } from 'app/acreditacion/services/error.service';
import { AuthService } from 'app/acreditacion/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class PermisosService {

  constructor(public authService: AuthService,
    public router: Router,
    private toastr: ToastrService,
    private masterService: MasterService,
    private error: ErrorService) { }

  ngOnInit() {

  }

  public validateUserByProcess(data:any){
    var user=this.authService.getLSuser();
    if(data.owner==user.username){
      return true;
    }
    return false;
  }

  verifyCodePermiso(roles:any[]){
   
    var user=this.authService.getLSuser();

    for(var i=0;i<roles.length;i++){
      if(user.username==roles[i].username){
        if(roles[i].code==3){
          return false;
        }
      }
    }
    return true;
  }

}
