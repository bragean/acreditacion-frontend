
export class UserType {
  constructor(
    public id?: number,
    public name?: string,
    public status?: string,
  ){}
}