import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FormPhaseComponent } from '../../process/form-phase/form-phase.component';
import { MasterService } from 'app/acreditacion/services/master.service';

@Component({
  selector: 'ms-form-criteria',
  templateUrl: './form-criteria.component.html',
  styleUrls: ['./form-criteria.component.scss']
})
export class FormCriteriaComponent implements OnInit {

  public form: FormGroup
  public id: any = ''

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<FormPhaseComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private masterService: MasterService) {
    this.form = this.fb.group({
      id: null,
      name: null,
      id_process: this.masterService.process.id,
      description: null,
      status: true
    })
  }

  ngOnInit() {
    this.initForm()
  }

  onCancel(): void {
    this.dialogRef.close('Cancel')
  }

  initForm() {
    if (this.data == null) {
      this.id = null

    }
    else {
      this.id = this.data.id
      this.form.setValue({
        id: this.data.id,
        name: this.data.name,
        description: this.data.description,
        id_process: Number(this.data.id_process),
        //finished: this.data.finished,
        status: this.data.status
      })
    }
  }

  onSubmit(data: any) {
    if (this.form.valid) {
      this.dialogRef.close(this.form.value)
      this.form.reset()
    }
  }
  
  onClose() {
    this.dialogRef.close('Close')
    this.form.reset()
  }

}
