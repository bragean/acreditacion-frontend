import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { AuthService } from 'app/acreditacion/services/auth.service';


@Component({
   selector: 'ms-loginV2-session',
   templateUrl: './loginV2-component.html',
   styleUrls: ['./loginV2-component.scss'],
   encapsulation: ViewEncapsulation.None
})
export class LoginV2Component {

   username: string = "";
   password: string = "";

   constructor(public authService: AuthService,
      public translate: TranslateService,
      public router: Router,
      private toastr: ToastrService) { }
   
   public onLogin(form) {
      if (form.valid) {
         return this.authService.loginuser(this.username, this.password).subscribe(
               data => {
                  this.authService.setToken(data.token)
                  this.authService.getUserServiceByToken(data.token).subscribe((response: any) => {
                     this.toastr.success("Bienvenido")
                     this.authService.setLSuser(response)
                     this.router.navigate(['/']);
                     
                  },error=>{
                     this.toastr.error("Error en el servidor",error.status)
                  })
               },
               error => {
                  this.toastr.error("Datos incorrectos","ERROR AL INICIAR SESIÓN")  
               }
            );
      }
   }
}



