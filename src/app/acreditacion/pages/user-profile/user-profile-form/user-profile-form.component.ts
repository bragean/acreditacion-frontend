import { MasterService } from './../../../services/master.service';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'ms-user-profile.form',
  templateUrl: './user-profile-form.component.html',
  styleUrls: ['./user-profile-form.component.scss']
})
export class UserProfileFormComponent implements OnInit {

  public form: FormGroup
  public id: any = ''
  usertypes: any[] = []

  types: any[]
  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<UserProfileFormComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private masterService: MasterService) {
    this.form = this.fb.group({
      first_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      photo: ['']

    })
  }

  ngOnInit() {
    this.initForm()

  }

  onCancel(): void {
    this.dialogRef.close('Cancel')
  }

  initForm() {

    this.form.setValue({

      first_name: this.data.first_name,
      last_name: this.data.last_name,
      photo: this.data.photo
    })
  }

  onSubmit(data: any) {
    if (this.form.valid) {
      console.log(data)
      this.dialogRef.close({
        first_name: data.first_name,
        last_name: data.last_name,
        photo: data.photo
      })
      this.form.reset()
    }
  }
  onClose() {
    this.dialogRef.close('Close')
    this.form.reset()
  }
  public onArchivoSeleccionado($event) {
    if ($event.target.files.length === 1) {
      this.form.get('photo').setValue($event.target.files[0])

    }
  }

}