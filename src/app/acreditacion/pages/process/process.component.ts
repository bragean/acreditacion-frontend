import { Router, ActivatedRoute } from '@angular/router';

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatDialog, MatSnackBar, MatTableDataSource, MatPaginatorIntl } from '@angular/material';
import { MasterService } from 'app/acreditacion/services/master.service';
import { DialogDeleteComponent } from 'app/acreditacion/dialog-delete/dialog-delete.component';
import { FormPhaseComponent } from './form-phase/form-phase.component';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { AuthService } from 'app/acreditacion/services/auth.service';
import {ErrorService} from 'app/acreditacion/services/error.service';
import { PermisosService } from 'app/acreditacion/services/permisos.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ms-process',
  templateUrl: './process.component.html',
  styleUrls: ['./process.component.scss']
})
export class ProcessComponent implements OnInit {


  @ViewChild(MatPaginatorIntl) paginator: MatPaginatorIntl;

  @ViewChild(MatSort) sort: MatSort

  isLoad: boolean = false


  displayedColumns: string[] = ['name', 'description', 'status_phase', 'create', 'finished', 'action'];
  dataSource: any
  id_process = ''
  name_process=''
  user
  listUsers: any[] = [];
  listPhases:any[]=[];


  constructor(private masterService: MasterService,
    private dialog: MatDialog, private snackBar: MatSnackBar,
    private route: Router,
    private error:ErrorService,
    private toastr: ToastrService, 
    private ar: ActivatedRoute, 
    public pageTitleService: PageTitleService,
    private authService: AuthService,
    private permiso:PermisosService
  ) { }

  ngOnInit() {
    this.configurePage();
    this.pageTitleService.setTitle( this.name_process)   
  }

  public configurePage(){
    if(this.masterService.process!=null){
      this.id_process = this.masterService.process.id;
      this.name_process = this.masterService.process.name;
      this.masterService.idprocess = this.id_process;
      this.user = this.authService.getLSuser()

      this.loadPhases(this.id_process)
      this.getUsers();
    }else{
      this.route.navigate(['evaluaciones'])
    }
  }

  getUsers() {
    this.masterService.getUsersRoleByProccess(this.id_process).subscribe((data: any) => {
      this.listUsers = data
      this.masterService.rol=this.permiso.verifyCodePermiso(this.listUsers);
    }, error => {
      this.error.getError(error)
    })
  }
  
  openPhase(data) {
    this.masterService.phase=data;
    this.route.navigate(['phase'])
  }

  applyFilter(filterValue: String) {
    this.dataSource.filter = filterValue.trim().toLowerCase()
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage()
    }
  }

  private loadPhases(id) {
    this.isLoad = true
    this.masterService.getPhasesByProcess(id).subscribe((data: any) => {
      this.listPhases=data;
      this.loadTask();

      this.dataSource = new MatTableDataSource(this.listPhases)
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.isLoad = false
    },
      error => {
        this.isLoad = false
        this.error.getError(error)
      })
  }

  loadTask() {

    for(var i=0;i<this.listPhases.length;i++){

      this.masterService.getTaskByPhase( this.listPhases[i].id).subscribe((data: any) => {     
        status=this.getProgressOfPhase(data);
        if(data.length!=0){
          for(var j=0;j<this.listPhases.length;j++){
            if(this.listPhases[j].id==data[0].id_phase){
              this.listPhases[j].status_phase=status;
            }
          }
        }        
      },
        error => {
          this.error.getError(error)
      })
    }
    
  }

  public getProgressOfPhase(tasks:any[]){
    var status;
    if(tasks.length==0){
      status=0;
    }else{
      var progress=0;
      tasks.forEach(element => {
        if(element.status_task==3){
          progress+=1;
        }
      });
      status=(Math.round((progress/tasks.length)*100)) 
    } 
    return status;
  }
  

  onSetPhase(data: any) {
    this.isLoad = true
    this.masterService.addPhase({
      id: data.id,
      name: data.name,
      id_process: Number(this.id_process),
      finished: data.finished,
      description: data.description,
      status: data.status
    }).subscribe(() => {
      this.loadPhases(this.id_process)
      this.isLoad = false
      this.toastr.success('Se añadió correctamente','ÉXITO')
    }, error => {
      this.isLoad = false
      this.error.getError(error)
    })
  }

  onEditPhase(data: any) {
    this.isLoad = true
    let send = {
      id: data.id,
      name: data.name,
      description: data.description,
      status: data.status
    }
    this.masterService.updatePhase(data).subscribe(() => {
      this.loadPhases(this.id_process)
      this.toastr.success('Se editó correctamente','ÉXITO')
    }, error => {
      this.isLoad = false
      this.error.getError(error)
    })
  }

  onUpdateStatus(data: any) {

    /* let status: string = (~~(!parseInt(data.status))).toString()
    let Phase1: Phase = new Phase(data.id,data.name, status)
    this.onEditPhase(Phase1) */
  }
  
  onDeletePhase(id: any) {
    this.isLoad = true
    this.masterService.deletePhase(id).subscribe(() => {
      this.loadPhases(this.id_process)
      this.isLoad = false
      this.toastr.success('Se eliminó correctamente','ÉXITO')
    }, error => {
      this.isLoad = false
      this.error.getError(error)
    })
  }

  openModal(data: any) {
    let rpv1: any
    if (data) {
      rpv1 = {
        id: data.id,
        name: data.name,
        finished: data.finished,
        description: data.description,
        status: data.status
      }
    } else {
      rpv1 = null
    }

    const dialogRef = this.dialog.open(FormPhaseComponent, {
      data: rpv1,
      width: '500px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result == 'Close') {
      } else {
        if (result.id == null) {
          this.onSetPhase(result)
        } else {
          this.onEditPhase(result)
        }
      }
    })
  }
  // pregunta de eliminar
  onDelete(i: any) {
    const dialogRef = this.dialog.open(DialogDeleteComponent, { data: "¿Estás seguro de que quieres eliminar esta fase de forma permanente? Se eliminarán las tareas asociadas", width: '450px' })

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        this.onDeletePhase(i.id)
      }
    })
  }

  //snackBar
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      verticalPosition: 'top'
    });
  }

  public checkRole(){
    
    return this.masterService.rol
  }

}