import { ToastrService } from 'ngx-toastr';
import { Component, OnInit,NgZone } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { AuthService } from 'app/acreditacion/services/auth.service';


@Component({
   selector: 'ms-restart-login',
   templateUrl: './restart-login.component.html',
   styleUrls: ['./restart-login.component.scss'],
   encapsulation: ViewEncapsulation.None
})
export class RestartLoginComponent {

   username: string = "";
   password: string = "";
   count=0;

   user: any

   constructor(public authService: AuthService,
      public translate: TranslateService,
      public router: Router,
      private toastr: ToastrService,
      private zone: NgZone) { }


   onLogin(form) {
      this.user=this.authService.getLSuser();

      if (form.valid) {
         if(this.username==this.user.username){
            return this.authService.loginuser(this.username, this.password).subscribe(data => {
               this.authService.setToken(data.token)
               this.authService.getUserServiceByToken(data.token).subscribe((response: any) => {
                  this.toastr.success("Bienvenido")
                  this.authService.setLSuser(response)
                  this.router.navigate(['/']);
                  this.zone.runOutsideAngular(() => {
                     location.reload();
                  });
                  this.count=0;
               },error=>{
                  this.toastr.error("Error en el servidor",error.status)
               })
            },
            error => {
               this.toastr.error('Datos incorrectos','ERROR Al INICIAR SESIÓN');
            });
         }else{
            this.toastr.error('Usuario incorrecto','ERROR AL REGRESAR A LA SESIÓN');

            if(this.count==3){
               this.count=0;
               localStorage.removeItem("user");
               localStorage.removeItem("accessToken");
               localStorage.removeItem("currentUser");
               this.router.navigate(['/session/loginV2']);
               this.zone.runOutsideAngular(() => {
                  location.reload();
               });                   
            }
            this.count+=1;
         }       
      }
   }

   public logout(){

      localStorage.removeItem("user");
      localStorage.removeItem("accessToken");
      localStorage.removeItem("currentUser");
      
      this.toastr.success("Cierre de sesión correcta!");
      this.router.navigate(['/session/loginV2']);
      this.zone.runOutsideAngular(() => {
         location.reload();
      });
   }
}

