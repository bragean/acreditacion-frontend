import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'ms-form-phase',
  templateUrl: './form-phase.component.html',
  styleUrls: ['./form-phase.component.scss']
})
export class FormPhaseComponent implements OnInit {

  public form: FormGroup
  public id: any = ''

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<FormPhaseComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.form = this.fb.group({
      id: null,
      name: null,
      description: null,
      status: true
    })
  }

  ngOnInit() {
    this.initForm()
  }

  onCancel(): void {
    this.dialogRef.close('Cancel')
  }

  initForm() {
    if (this.data == null) {
      this.id = null

    }
    else {
      this.id = this.data.id
      this.form.setValue({
        id: this.data.id,
        name: this.data.name,
        description: this.data.description,
        status: this.data.status
      })
    }
  }

  onSubmit(data: any) {
    if (this.form.valid) {

      this.dialogRef.close({
        id: this.form.value.id,
        name: this.form.value.name,
        description: this.form.value.description,
        status: this.form.value.status
      })
      this.form.reset()
    }
  }
  onClose() {
    this.dialogRef.close('Close')
    this.form.reset()
  }

}