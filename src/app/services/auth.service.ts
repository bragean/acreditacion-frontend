import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomainService } from 'app/acreditacion/services/domain.service';
import { Observable } from 'rxjs';
import { isNullOrUndefined } from 'util';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  constructor(private htttp: HttpClient, private dom: DomainService) { }
  headers: HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
    Authorization: 'Bearer ' + this.getToken()
  });


  /*   registerUser(name: string, email: string, password: string) {
      const url_api = "http://localhost:3000/api/Users";
      return this.htttp
        .post(
          url_api,
          {
            name: name,
            email: email,
            password: password
          },
          { headers: this.headers }
        )
        .pipe(map(data => data));
    }
   */
  loginuser(username: string, password: string): Observable<any> {
    const url_api = this.dom.getDomain() + "auth/login/";
    return this.htttp
      .post(
        url_api,
        { username, password }
      )
      .pipe(map(data => data));
  }
  setLSuser(data: any) {
    localStorage.setItem("user", JSON.stringify(data));
  }
  getLSuser(data: any) {
    let user_string = localStorage.getItem("user");
    if (!isNullOrUndefined(user_string)) {
      let user = JSON.parse(user_string);
      return user;
    } else {
      return null;
    }
  }

  setUser(user: any): void {
    let user_string = JSON.stringify(user);
    localStorage.setItem("currentUser", user_string);
  }

  setToken(token): void {
    localStorage.setItem("accessToken", token);
  }

  getToken() {
    return localStorage.getItem("accessToken");
  }

  getCurrentUser() {
    let user_string = localStorage.getItem("currentUser");
    if (!isNullOrUndefined(user_string)) {
      let user = JSON.parse(user_string);

      return user;
    } else {
      return null;
    }
  }

  logoutUser() {
    let accessToken = localStorage.getItem("accessToken");
    let headers2: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + accessToken
    })
    const url_api = this.dom.getDomain() + "api/main/mobility/logout/"
    localStorage.removeItem("accessToken");
    localStorage.removeItem("currentUser");
    localStorage.removeItem("user");
    return this.htttp.get(url_api, { headers: headers2 });
  }

  getPermission() {
    let array: Array<any> = this.getCurrentUser().permissions
    return array;
  }
  getAuth(modulo: string, perm: any): boolean {
    if (this.getCurrentUser().is_superuser == 'True') {
      return true
    } else {
      let acc = this.getPermission().find(el => {
        return el.name == modulo
      })
      console.log(acc.result.split(',')[perm - 1])
      return acc.result.split(',')[perm - 1] == '1'
    }
  }
  getUserServiceByToken(token) {

    let headers: HttpHeaders = new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: 'Bearer ' + token
    });
    const url_api = this.dom.getDomain() + "api/user/get/select/";
    return this.htttp.get(url_api, { headers: headers })
  }

  //validar para la descarga 
  getUserValidate(username, password) {
    const url_api = this.dom.getDomain() + "api/main/mobility/get/user/"
    return this.htttp
      .post(
        url_api,
        { username, password },
        { headers: this.headers }
      )
      .pipe(map(data => data));
  }
}