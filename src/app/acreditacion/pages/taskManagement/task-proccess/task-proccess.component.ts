import { DomainService } from './../../../services/domain.service';
import { ActivatedRoute } from '@angular/router';
import { MasterService } from './../../../services/master.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginatorIntl, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { AuthService } from 'app/service/auth-service/auth.service';
import { ToolsComponent } from '../tools/tools.component';
import {ErrorService} from 'app/acreditacion/services/error.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'ms-task-proccess',
  templateUrl: './task-proccess.component.html',
  styleUrls: ['./task-proccess.component.scss']
})
export class TaskProccessComponent implements OnInit {
  noDataProvider: boolean = true
  displayedColumns: string[] = ['name','status', 'action'];
  searchData: string
  public dataUsers: any[]
  public dataTable: any[]

  files: any = [];
  objFilesList: any = [];

  task: any = {
    id: '',
    id_indicator: "",
    id_phase: "",
    name: "",
    description: "",
    end_date: "",
    user_list: "",
    close_date: '',
    status_delivery: "",
    status_review: "",
    status_task: 0,
    status: "",
    created: "",
    last_updated: ""
  }
  id_task: ''
  option:any= '' // user: al que se les asigno // owner: para revisar
  nameList: any[] = []

  @ViewChild(MatSort) sort: MatSort;


  dataSource = new MatTableDataSource([])
  isLoad = false
  listDocs: any;

  numOrder: any
  user: any

  res = 0

  id_phase:any=""


  constructor(
    private title: PageTitleService,
    private masterService: MasterService,
    private activate: ActivatedRoute,
    private dom: DomainService,
    private userservice: AuthService,
    private dialog: MatDialog,
    private error:ErrorService,
    private toastr: ToastrService,
    private route: Router,
  ) {
    this.id_task = this.activate.snapshot.params['id']
    this.option = this.activate.snapshot.params['option']
    this.user = this.userservice.getLSuser()
  }

  ngOnInit() {
    if(this.masterService.idphasetask!=null){
      this.id_phase=this.masterService.idphasetask;
      this.gettaskById()
      this.getDetailByTask()
      this.getDocsV2ByTask()
    }else{
      this.route.navigate(["task-list"]);
    }
    

  }

  uploadFile(event) {
    if (this.files.length > 5 || event.length > 5) {
      //this.toastr.info("Excedió con la cantidad de archivos")
      return
    }
    for (let index = 0; index < event.length; index++) {
      const element = event[index];
      this.files.push(element.name)
      this.objFilesList.push(element)
    }
  }
  deleteAttachment(index) {
    this.files.splice(index, 1)
    this.objFilesList.splice(index, 1)
  }
  gettaskById() {
    /*
    this.masterService.getTaskByid(this.id_task).subscribe(data => {
      console.log(data)
      this.task = data
      this.getResto()
      this.title.setTitle("Tarea: " + this.task.name)
    },error=>{
      this.error.getError(error)
    })*/
    this.masterService.getTaskByPhase(this.id_phase).subscribe((data: any) => {
      console.log(data)

      for(var i=0;i<data.length;i++){
        if(data[i].id==this.id_task){
          this.task=data[i];
          
        }    
      }
      this.getResto()
      this.title.setTitle("Tarea: " + this.task.name)
    },
      error => {
        this.error.getError(error)
      })

    
  }
  getDetailByTask() {
    this.nameList = []
    this.masterService.getListDocsByIdTask(this.id_task).subscribe((data: any) => {
      this.listDocs = data
      this.listDocs.forEach(element => {
        this.nameList.push({ name: '', status: false })
      });
    },error=>{
      this.error.getError(error)
    })
  }

  getRestante(date) {
    let i = new Date()
    let d = new Date(date)
    var diaEnMils = 1000 * 60 * 60 * 24,
      diff = d.getTime() - i.getTime() + diaEnMils;// +1 incluir el dia de ini
    return (diff / diaEnMils);
  }
  downLoadFormat(data) {
    return this.dom.getDomain().substring(0, this.dom.getDomain().length - 1) + data
  }

  onFileChange(event, i) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      //this.nameDoc = file.name
      this.listDocs[i].file_format_full = file
      this.nameList[i].name = file.name
    }
  }

  updateDetail(item, i) {

    let formDta = new FormData()

    formDta.append("file_format_full", item.file_format_full)
    formDta.append("comment", item.comment)
    formDta.append("id_task", this.task.id)

    this.masterService.updateTaskDetail(formDta, item.id).subscribe((data: any) => {
      this.nameList[i].status = true
      let sep = 100 / this.listDocs.length

      let send = (i + 1) * sep
      this.updateTask(send)
    },error=>{
      this.error.getError(error)
    })
  }

  updateTask(task) {
    let statuss = 1
    if (task == 100) {
      statuss = 2
    }
    let data = {
      close_date: null,
      status_delivery: statuss,
      status_task: task,
    }
    this.masterService.updateTask(data, this.task.id).subscribe(data => {
    },error=>{
      this.error.getError(error)
    })

  }

  openTools() {

    const dialogRef = this.dialog.open(ToolsComponent, {
      data: "ejemplo",
      width: '700px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result == 'Close') {
      } else {

      }
    })
  }

  getResto() {
    var diff = new Date(this.task.end_date).getTime() - new Date().getTime();
    this.res = Math.round(diff / (1000 * 60 * 60 * 24))
  }

  onSaveDocuments() {
    this.objFilesList.forEach(element => {
      let formData = new FormData()
      formData.append('document', element)
      formData.append('id_task', this.task.id)
      this.masterService.setDocV2ByTask(formData).subscribe((data: any) => {
        this.ngOnInit()
        this.toastr.success("Documentos guardados")
        
      },error=>{
        this.error.getError(error)
      })
    });
  

    this.masterService.updateTask({
      name: this.task.name,
      end_date: this.task.end_date,
      status_task:'1',
      user_list: this.task.user_list.toString(),
      description: this.task.description,
    }, this.task.id).subscribe(() => {
      //this.ngOnInit()
      this.files=[];

    }, error => {
      this.error.getError(error)
      this.isLoad = false


    })
  }

  getDocsV2ByTask() {
    this.nameList = []
    this.masterService.getListDocsV2ByIdTask(this.id_task).subscribe((data: any) => {
      this.dataSource.data = data
    },error=>{
      this.error.getError(error)
    })
  }
  closeTask() {
    this.masterService.updateTask({
      name: this.task.name,
      end_date: this.task.end_date,
      user_list: this.task.user_list,
      description: this.task.description,
      status_task: 2
    }, this.task.id).subscribe(() => {
      this.toastr.success("Tarea completada")
      this.ngOnInit()

    }, error => {
      this.error.getError(error)
      this.isLoad = false
    })
  }

  closeTaskReview() {
    this.masterService.updateTask({
      name: this.task.name,
      end_date: this.task.end_date,
      user_list: this.task.user_list,
      description: this.task.description,
      status_task: 3
    }, this.task.id).subscribe(() => {
      this.toastr.success("Tarea cerrada")
      this.ngOnInit()

    }, error => {
      this.error.getError(error)
      this.isLoad = false
    })
  }

  openTask() {
    this.masterService.updateTask({
      name: this.task.name,
      end_date: this.task.end_date,
      user_list: this.task.user_list,
      description: this.task.description,
      status_task: 1
    }, this.task.id).subscribe(() => {
      this.toastr.success("Tarea abierta")
      this.ngOnInit()

    }, error => {
      this.error.getError(error)
      this.isLoad = false
    })
  }

  // obtener el nombre del doc 

  getName(data) {

    let name = data.split('/')
    return name[name.length - 1]
  }

  checkTypeTask(){
    if(this.option=="owner"){
      if(this.task.status_task=='3'){
        return false;
      }
      return true;
    }
    return false;
  }
  checkStatusTask(){
    if(this.task.status_task=='3'){
      return false;
    }
    return true;
  }

  checkDate(){
      var task_date=this.task.end_date.toString().split("T",1);
      var date=new Date();
      var actual_date=date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
      if(task_date>actual_date){
        if(this.task.status_task=='3'){
          return false;
        }
        return true;
      }
      return false;
  }

}
