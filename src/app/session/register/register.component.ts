import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AuthService } from 'app/acreditacion/services/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
	selector: 'ms-register-session',
	templateUrl: './register-component.html',
	styleUrls: ['./register-component.scss'],
	encapsulation: ViewEncapsulation.None,
})

export class RegisterComponent {

	first_name: string;
	last_name: string;
	username: string;
	password: string;
	passwordConfirm: string;

	constructor(public authService: AuthService,
		public translate: TranslateService,
		private toastr: ToastrService,
		private router: Router
	) { }

	//register method is used to sign up on the template.
	register(value) {
		let send = {
			first_name: this.first_name,
			last_name: this.last_name,
			username: this.username,
			password: this.password,

		}

		this.authService.registerUser(send).subscribe((res: any) => {
			this.toastr.success("Se registró correctamente.")
			this.router.navigate(["/session/loginV2"])
		}, error => {
			this.toastr.error("Error al registrarse")
		})
	}

	verificarContrasena() {

		if (this.password == this.passwordConfirm) {
			this.toastr.success("La contraseña es válida")
		} else {
			this.toastr.error("La contraseña no coincide")
		}
	}

}



