import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomainService } from 'app/acreditacion/services/domain.service';
import { isNullOrUndefined } from 'util';

@Injectable({
   providedIn: 'root'
})
export class AuthService {

   user: Observable<firebase.User>;
   userData: any;
   isLoggedIn = false;

   constructor(private firebaseAuth: AngularFireAuth,
      private router: Router,
      private toastr: ToastrService,
      private http: HttpClient, private dom: DomainService,
      private zone: NgZone
   ) {
      this.user = firebaseAuth.authState;
   }

   headers: HttpHeaders = new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: 'Bearer ' + this.getToken()
   });

   getToken() {
      return localStorage.getItem("accessToken");
   }

   /*
    *  getLocalStorageUser function is used to get local user profile data.
    */
   setLSuser(data: any) {
      localStorage.setItem("user", JSON.stringify(data));
   }
   getLSuser() {
      let user_string = localStorage.getItem("user");
      if (!isNullOrUndefined(user_string)) {
         let user = JSON.parse(user_string);
         return user;
      } else {
         return null;
      }
   }
   getCurrentUser() {
      let user_string = localStorage.getItem("user");
      if (!isNullOrUndefined(user_string)) {
         let user = JSON.parse(user_string);
         return user;
      } else {
         return null;
      }
   }

   /*    getLocalStorageUser(){
         this.userData = JSON.parse(localStorage.getItem("userProfile"));
         if(this.userData) {
            this.isLoggedIn = true;
            return true;
         } else {
            this.isLoggedIn = false;
            return false;
         }    
      } */

   /*

/*    signupUserProfile(value) {
      this.firebaseAuth
         .auth
         .createUserWithEmailAndPassword(value.email, value.password)
         .then(value => {
            this.toastr.success('Successfully Signed Up!');
            this.setLocalUserProfile(value);
            this.router.navigate(['/']);
         })
         .catch(err => {
            this.toastr.error(err.message);
         });
   } */

   registerUser(user: any) {
      const url_api = this.dom.getDomain() + "register/";
      return this.http
         .post(
            url_api,
            user,
            { headers: this.headers }
         )
         .pipe(map(data => data));
   }

   /*
    * loginUser fuction used to login 
    */
   loginUser(value) {
      this.firebaseAuth
         .auth
         .signInWithEmailAndPassword(value.email, value.password)
         .then(value => {
            this.setLocalUserProfile(value);
            this.toastr.success('Successfully Logged In!');
            this.router.navigate(['/']);
         })
         .catch(err => {
            this.toastr.error(err.message);
         });
   }

   loginuser(username: string, password: string): Observable<any> {
      const url_api = this.dom.getDomain() + "api/login/";
      return this.http
         .post(
            url_api,
            { username, password },
            { headers: this.headers }
         )
         .pipe(map(data => data));
   }

   /*
    * resetPassword is used to reset your password
    */
   resetPassword(value) {
      this.firebaseAuth.auth.sendPasswordResetEmail(value.email)
         .then(value => {
            this.toastr.success("A password reset link has been sent to this email.");
            this.router.navigate(['/session/login']);
         })
         .catch(err => {
            this.toastr.error(err.message);
         });
   }


   /*
    * resetPasswordV2 is used to reset your password
    */
   resetPasswordV2(value) {
      this.firebaseAuth.auth.sendPasswordResetEmail(value.email)
         .then(value => {
            this.toastr.success("A password reset link has been sent to this email.");
            this.router.navigate(['/session/loginV2']);
         })
         .catch(err => {
            this.toastr.error(err.message);
         });
   }

   /*
    * logOut function is used to sign out  
    */
   logOut() {
      this.firebaseAuth
         .auth
         .signOut();
      localStorage.removeItem("userProfile");
      localStorage.removeItem("user");
      localStorage.removeItem("accessToken");

      this.isLoggedIn = false;
      this.toastr.success("Successfully logged out!");
      this.router.navigate(['/session/loginV2']);
      
      this.zone.runOutsideAngular(() => {
         location.reload();
     });
   }

   /*
    * setLocalUserProfile function is used to set local user profile data.
    */
   setLocalUserProfile(value) {
      localStorage.setItem("userProfile", JSON.stringify(value));
      // this.getLocalStorageUser();
      this.isLoggedIn = true;
   }
   setToken(value) {
      localStorage.setItem("token", JSON.stringify(value))
   }
}
