import { MasterService } from 'app/acreditacion/services/master.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'app/service/auth-service/auth.service';
import {ErrorService} from 'app/acreditacion/services/error.service';

@Component({
  selector: 'ms-list-task',
  templateUrl: './list-task.component.html',
  styleUrls: ['./list-task.component.scss']
})
export class ListTaskComponent implements OnInit {


  filter = {
    id_process: '',
    id_phase: '',
    option: '',
  }
  user: any

  comboProcess: any[] = []
  comboPhase: any[] = []

  taskList: any[] = []

  constructor(
    private route: Router,
    private title: PageTitleService,
    private masterService: MasterService,
    private toastr: ToastrService,
    private userservice: AuthService,
    private error:ErrorService
  ) {
    this.user = this.userservice.getLSuser()
  }

  ngOnInit() {
    this.title.setTitle("Lista de Tareas")

    this.getProcessBYUser()

  }

  verTarea(id) {
    this.route.navigate(["/task-process/" + id + "/" + this.filter.option])
    this.masterService.idphasetask=this.filter.id_phase;
  }

  getProcessBYUser() {
    this.masterService.getProcesssByUser().subscribe((data: any) => {
      this.comboProcess = data
    },error=>{
      this.error.getError(error)
    })

  }

  getPhasesByProcess(id) {
    this.masterService.getPhasesByProcess(id).subscribe((data: any) => {
      this.comboPhase = data
    },error=>{
      this.error.getError(error)
    })
  }

  getTaskByphase() {
    if(this.filter.option=='user'){
      this.masterService.getTaskByPhase(this.filter.id_phase).subscribe((data: any) => {
        console.log(data)
        var listTasks:any[]=[]

        for(var i=0;i<data.length;i++){
          var user_list:any[]=[];
          if(data[i].user_list!=null){
            data[i].user_list=data[i].user_list.toString().split(',')
          }
          user_list=data[i].user_list;
          console.log(user_list)    
          if(user_list!=null){
            for(var j=0;j<user_list.length;j++){
              if(this.user.username==user_list[j]){
                listTasks.push(data[i]);
              }
            }
          }
          console.log("next")       
        }
       this.taskList=listTasks;
  
      },
        error => {
          this.error.getError(error)
        })
    }
    
    if(this.filter.option=='owner'){
      this.masterService.getTaskBYphaseAndIdUser(this.filter.id_phase, this.filter.option).subscribe((data: any) => {
        console.log(data)
        if (Array.isArray(data)) {
          this.taskList = data
        } else {
          alert(data.status)
        }
      },error=>{
        this.error.getError(error)
      })
    }
  }
  
  getTaskByAdmin(id) {
    this.masterService.getTaskByAdmin(id).subscribe((data: any) => {
      if (Array.isArray(data)) {
        this.taskList = data
      } else {
        alert(data.status)
      }

    },error=>{
      this.error.getError(error)
    })
  }
}
