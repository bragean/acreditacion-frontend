import { DomainService } from 'app/acreditacion/services/domain.service';
import { Document } from './../../../models/document';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FormPhaseComponent } from '../../process/form-phase/form-phase.component';
import { MasterService } from 'app/acreditacion/services/master.service';
@Component({
  selector: 'ms-form-task',
  templateUrl: './form-task-management.component.html',
  styleUrls: ['./form-task-management.component.scss']
})
export class FormTaskManagementComponent implements OnInit {

  public form: FormGroup
  public id: any = ''

  dataSource = new MatTableDataSource([])
  displayedColumns: any[] = ["id", "name", "action"]

  nameDoc: any = ''

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<FormPhaseComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private masterService: MasterService, private dom: DomainService) {
    this.form = this.fb.group({
      id: null,
      id_task: null,
      name: null,
      file_format: null,
      file_format_full: null,
      comment: null,
    })
  }

  ngOnInit() {
    this.initForm()

  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.nameDoc = file.name
      this.form.get("file_format").setValue(file);
    }
  }



  onCancel(): void {
    this.dialogRef.close('Cancel')
  }

  initForm() {
    if (this.data == null) {
      this.id = null

    } else {
      this.id = this.data.id
      this.form.get('id_task').setValue(parseInt(this.data.id))
      this.getDocsByTaskId(this.data.id)
    }
  }

  getDocsByTaskId(id) {
    this.masterService.getListDocsByIdTask(id).subscribe((data: any) => {
      this.dataSource.data = data
    })
  }



  onSubmit(data: any) {
    if (this.form.valid) {
      this.setDocument({
        file_format: this.form.value.file_format,
        name: this.form.value.name,
        id_task: this.form.value.id_task
      })

    }
  }
  onClose() {
    this.dialogRef.close('Close')
    this.form.reset()
  }
  setDocument(file: any) {
    let formData = new FormData()
    formData.append('file_format', file.file_format)
    formData.append('name', file.name)
    formData.append('id_task', file.id_task)

    this.masterService.setDocByTask(formData).subscribe(data => {
      this.form = this.fb.group({
        id: null,
        id_task: null,
        name: null,
        file_format: null,
        file_format_full: null,
        comment: null,
      })
      this.getDocsByTaskId(this.id)
      this.nameDoc=""
    }, error => {
      console.log(error)
    })
  }
  ondelete(id) {
    this.masterService.deleteDocDetail(id).subscribe((data: any) => {
      this.getDocsByTaskId(this.id)
    })
  }
  downLoadFormat(data) {
    return this.dom.getDomain().substring(0, this.dom.getDomain().length - 1) + data
  }


}

