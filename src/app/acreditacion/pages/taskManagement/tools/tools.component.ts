import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { MasterService } from 'app/acreditacion/services/master.service';

@Component({
  selector: 'ms-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.scss']
})
export class ToolsComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ToolsComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private masterService: MasterService) { }

  ngOnInit() {
  }
  onCancel(): void {
    this.dialogRef.close('Cancel')
  }
}
