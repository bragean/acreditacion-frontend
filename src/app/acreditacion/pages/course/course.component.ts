import { Component, OnInit, ViewChild } from '@angular/core';
import { DialogDeleteComponent } from 'app/acreditacion/dialog-delete/dialog-delete.component';
import { MatTableDataSource, MatSnackBar, MatDialog, MatPaginatorIntl, MatSort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterService } from 'app/acreditacion/services/master.service';
import { DialogComponent } from 'app/acreditacion/dialog/dialog.component';
import { AuthService } from 'app/service/auth-service/auth.service';
import { FormCompetenceComponent } from './form-competence/form-competence.component';
import {ErrorService} from 'app/acreditacion/services/error.service';
import { ToastrService } from 'ngx-toastr';
import { PageTitleService } from 'app/core/page-title/page-title.service';

@Component({
  selector: 'ms-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss']
})
export class CourseComponent implements OnInit {

  @ViewChild(MatPaginatorIntl) paginator: MatPaginatorIntl;

  @ViewChild(MatSort) sort: MatSort

  isLoad: boolean = false
  

  //id_process = ''
  idphase = ''
  user

  /* Desde aqui son los cambios */
  displayedColumns: string[] = ['name', 'reference', 'action'];
  dataSource: any
  listCompetences=[{name:"habilidad para culaquier cosa",reference:"referencia.html"}]

  displayedColumnsRubric: string[] = ['evidence', 'appearance', 'criteria_general','criteria_course','level'];
  datasourceEvidence:any
  listEvidences=[{evidence:"Evidencia 1",appearance:"Aspecto a evaluar",criteria_general:"Criterio general",criteria_course:"Criterio de la asignatura",level:"Niveles de desempeño"}]
  id_course = ''
  name_course=''

  course:any

  constructor(private masterService: MasterService,
    private dialog: MatDialog, private snackBar: MatSnackBar,
    private route: Router, private ar: ActivatedRoute,
    private authService: AuthService,
    private error:ErrorService,
    private toastr: ToastrService,
    public pageTitleService: PageTitleService 
  ) {
    this.id_course = this.ar.snapshot.paramMap.get('id')
    this.name_course = this.ar.snapshot.paramMap.get('name')
    
  }

  ngOnInit() {
    this.pageTitleService.setTitle(this.name_course)
    this.getCourseById()
    this.loadCompetences()
    this.loadEvidences()

  }

  applyFilter(filterValue: String) {
    this.dataSource.filter = filterValue.trim().toLowerCase()
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage()
    }
  }

  public getCourseById(){
    this.course={name:'Matemática',type:'Avanzado',description:'Curso de matemática'}
  }

  public loadCompetences(){
    this.dataSource = new MatTableDataSource(this.listCompetences)
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  public loadEvidences(){
    this.datasourceEvidence = new MatTableDataSource(this.listEvidences)
    this.datasourceEvidence.sort = this.sort;
    this.datasourceEvidence.paginator = this.paginator;
  }

  /* Para abajo de plantilla*/

  private loadTasks() {

    this.isLoad = true
    /*     this.masterService.getTasks().subscribe((data: any) => { */
    this.masterService.getTaskByPhase(this.idphase).subscribe((data: any) => {
      /* this.nts = data
      this.nts.sort(function (obj1, obj2) {
        return obj2.id - obj1.id
      }) */
      console.log("tareasss", data)
      this.dataSource = new MatTableDataSource(data)
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.isLoad = false
    },
      error => {
        this.isLoad = false
        this.error.getError(error)
      })
  }
  onSetTask(data: any) {
    console.log(data)
    this.isLoad = true

    console.log("envio", data)
    this.masterService.addTask({

      id_indicator: data.id_indicator,
      id_criteria: data.id_criteria,
      name: data.name,

      end_date: data.end_date,
      id_phase: Number(this.idphase),

      user_list: data.user_list,
      description: data.description,

    }
    ).subscribe(() => {
      this.loadTasks()
      this.isLoad = false
      this.toastr.success('Se añadió correctamente','ÉXITO')
    }, error => {
      this.isLoad = false
      this.error.getError(error)
    })
  }

  onEditTask(data: any) {
    this.isLoad = true
    console.log(data)
    this.masterService.updateTask({


      name: data.name,
      end_date: data.end_date,

      user_list: data.user_list,
      description: data.description,


    }, data.id).subscribe(() => {
      this.loadTasks()
      this.toastr.success('Se editó correctamente','ÉXITO')
    }, error => {
      this.isLoad = false
      this.error.getError(error)

    })
  }

  onUpdateStatus(data: any, id) {
    this.masterService.setStatusTask(data, id).subscribe(() => {
      this.loadTasks()
      this.toastr.success('Se editó correctamente','ÉXITO')
    }, error => {
      this.isLoad = false
      this.error.getError(error)
    })

  }
  onDeleteTask(id: any) {
    this.isLoad = true
    this.masterService.deleteTask(id).subscribe(() => {
      this.loadTasks()
      this.isLoad = false
      this.toastr.success('Se eliminó correctamente','ÉXITO')
    }, error => {
      this.error.getError(error)
      this.isLoad = false

    })
  }

  openModal(data: any) {


    const dialogRef = this.dialog.open(FormCompetenceComponent, {
      data: data,
      width: '700px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result == 'Close') {
        console.log('close')
      } else {
        if (result.id == null) {

          this.onSetTask(result)
        } else {
          this.onEditTask(result)
        }
      }
    })
  }
  // pregunta de eliminar
  onDelete(i: any) {
    const dialogRef = this.dialog.open(DialogDeleteComponent, { data: "¿Estás seguro de que quieres eliminar  de forma permanente?", width: '450px' })

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        this.onDeleteTask(i.id)
      }
    })
  }

  onSetStatus(element, i: any) {
    let str = i == '1' ? 'En Proceso' : 'Realizada'
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: "Cambiar estado de una tarea",
        content: `¿Estas seguro que deseas marcar esta tarea como '${str}'? `
      }, width: '450px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        let obj: any = new Object(element)
        //this.onEditTask(obj)
        this.onUpdateStatus({ status_task: i }, element.id)
      }
    })
  }

  closeFase() {
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: "¿Seguro que desea cerrar la fase?",
        content: "Se enviara un correo a los usuarios"
      }, width: '450px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        this.onsetNotification()
      }
    })
  }
  onsetNotification() {
    this.isLoad = true
    this.masterService.sendNotification().subscribe(() => {
      this.loadTasks()
      this.isLoad = false
      this.toastr.success('Enviado','ÉXITO')
    }, error => {
      this.isLoad = false
      this.error.getError(error)

    })
  }

  //snackBar
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      verticalPosition: 'top'
    });
  }

  changeDate(element) {
    return (new Date(element)).getFullYear() + '-' + (new Date(element)).getMonth() + '-' + (new Date(element)).getDay()
  }
}