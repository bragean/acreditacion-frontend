export class User {
  constructor(
    public id?: number,
    public first_name?: string,
    public last_name?: string,
    public username?:string,
    public password?: string,
    public id_usertype?: string,
    public status?: string,
  ){}
}