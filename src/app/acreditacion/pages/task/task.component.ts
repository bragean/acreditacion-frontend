import { Component, OnInit, ViewChild } from '@angular/core';
import { DialogDeleteComponent } from 'app/acreditacion/dialog-delete/dialog-delete.component';
import { MatTableDataSource, MatSnackBar, MatDialog, MatPaginatorIntl, MatSort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterService } from 'app/acreditacion/services/master.service';
import { FormTaskManagementComponent } from './form-task-management/form-task-management.component';
import { DialogComponent } from 'app/acreditacion/dialog/dialog.component';
import { AuthService } from 'app/acreditacion/services/auth.service';
import { FormTaskComponent } from './form-task/form-task.component';
import {ErrorService} from 'app/acreditacion/services/error.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ms-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {

  @ViewChild(MatPaginatorIntl) paginator: MatPaginatorIntl;

  @ViewChild(MatSort) sort: MatSort

  isLoad: boolean = false

  displayedColumns: string[] = ['name', 'description', 'start_date', 'end_date', 'status', 'action'];
  dataSource: any

  //id_process = ''
  idphase = ''

  id_criteria=''
  id_indicator=''

  indicators: any[] = []
  criterias: any[] = []

  projectStatus: any[] = []

  user

  constructor(private masterService: MasterService,
    private dialog: MatDialog, private snackBar: MatSnackBar,
    private route: Router, private ar: ActivatedRoute,
    private authService: AuthService,
    private error:ErrorService,
    private toastr: ToastrService, 
  ) {
    this.user = this.authService.getLSuser()
  }

  ngOnInit() {
      this.idphase = this.masterService.idphases
      this.loadCriterias()
   
  }

  openTask(data) {
    this.route.navigate(['task/' + data.id])
  }

  applyFilter(filterValue: String) {
    this.dataSource.filter = filterValue.trim().toLowerCase()
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage()
    }
  }

  public getTasks(){
    this.loadTasks()
  }

  private loadTasks() {

    this.isLoad = true

    this.masterService.getTaskByPhase(this.idphase).subscribe((data: any) => {
      console.log(data)
      var listTasks:any[]=[]
      for(var i=0;i<data.length;i++){
        if(this.id_criteria==data[i].id_criteria && this.id_indicator==data[i].id_indicator){

          listTasks.push(data[i])
        }
      }

      this.dataSource = new MatTableDataSource(listTasks)
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.isLoad = false
    },
      error => {
        this.isLoad = false
        this.error.getError(error)
      })
  }
  onSetTask(data: any) {

    this.isLoad = true

    this.masterService.addTask({

      id_indicator: data.id_indicator,
      id_criteria: data.id_criteria,
      name: data.name,

      end_date: data.end_date,
      id_phase: Number(this.idphase),
      status_task:data.status_task,
      user_list: data.user_list,
      description: data.description,

    }
    ).subscribe(() => {
      this.loadTasks()
      this.isLoad = false
      this.toastr.success('Se añadió correctamente','ÉXITO')
    }, error => {
      this.isLoad = false
      this.error.getError(error)
    })
  }

  onEditTask(data: any) {
    this.isLoad = true
    this.masterService.updateTask({
      name: data.name,
      end_date: data.end_date,
      status_task:data.status_task,
      user_list: data.user_list,
      description: data.description,

    }, data.id).subscribe(() => {
      this.loadTasks()
      this.toastr.success('Se editó correctamente','ÉXITO')
    }, error => {
      this.isLoad = false
      this.error.getError(error)

    })
  }

  onUpdateStatus(data: any, id) {
    this.masterService.setStatusTask(data, id).subscribe(() => {
      this.loadTasks()
      this.toastr.success('Se editó correctamente','ÉXITO')
    }, error => {
      this.isLoad = false
      this.error.getError(error)
    })

  }
  onDeleteTask(id: any) {
    this.isLoad = true
    this.masterService.deleteTask(id).subscribe(() => {
      this.loadTasks()
      this.isLoad = false
      this.toastr.success('Se eliminó correctamente','ÉXITO')
    }, error => {
      this.error.getError(error)
      this.isLoad = false

    })
  }

  openModalTask(data: any) {
    let rpv1: any
    if (data) {
      rpv1 = {
        id: data.id,
        id_indicator: data.id_indicator,
        name: data.name,
        start_date: data.start_date,
        end_date: data.end_date,
        //close_date: data.close_date,
        id_criteria: data.id_criteria,
        status_task: data.status_task,
        user_list: data.user_list,
        description: data.description,
        status: data.status,
        close_date: data.close_date,
        status_delivery: data.status_delivery,
        status_review: data.status_review,
      }
    } else {
      rpv1 = null
    }

    const dialogRef = this.dialog.open(FormTaskManagementComponent, {
      data: rpv1,
      width: '700px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result == 'Close') {
      } else {

      }
    })
  }

  openModal(data: any) {

    const dialogRef = this.dialog.open(FormTaskComponent, {
      data: data,
      width: '700px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result == 'Close') {
      } else {
        if (result.id == null) {

          this.onSetTask(result)
        } else {
          this.onEditTask(result)
        }
      }
    })
  }
  // pregunta de eliminar
  onDelete(i: any) {
    const dialogRef = this.dialog.open(DialogDeleteComponent, { data: "¿Estás seguro de que quieres eliminar  de forma permanente?", width: '450px' })

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        this.onDeleteTask(i.id)
      }
    })
  }

  onSetStatus(element, i: any) {
    let str = i == '1' ? 'En Proceso' : 'Realizada'
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: "Cambiar estado de una tarea",
        content: `¿Estas seguro que deseas marcar esta tarea como '${str}'? `
      }, width: '450px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        let obj: any = new Object(element)
        this.onUpdateStatus({ status_task: i }, element.id)
      }
    })
  }

  closeFase() {
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: "¿Seguro que desea cerrar la fase?",
        content: "Se enviara un correo a los usuarios"
      }, width: '450px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        this.onsetNotification()
      }
    })
  }
  onsetNotification() {
    this.isLoad = true
    this.masterService.sendNotification().subscribe(() => {
      this.loadTasks()
      this.isLoad = false
      this.toastr.success('Enviado','ÉXITO')
    }, error => {
      this.isLoad = false
      this.error.getError(error)

    })
  }

  //snackBar
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      verticalPosition: 'top'
    });
  }

  changeDate(element) {
    return (new Date(element)).getFullYear() + '-' + (new Date(element)).getMonth() + '-' + (new Date(element)).getDay()
  }

  loadCriterias() {

    this.masterService.getCriteriasByProcess(this.masterService.idprocess).subscribe((data: any) => {
      this.criterias = data

    },
      error => {
        this.error.getError(error)
      })
  }

  selectIndicatorByCriterio(data) {
    this.masterService.getIndicatorsByCriteria(this.id_criteria).subscribe((data: any) => {
      this.indicators = data
    },error=>{
      this.error.getError(error)
    })
  }

  public checkRole(){
    return this.masterService.rol;
  }

}