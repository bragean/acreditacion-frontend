import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import 'hammerjs';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';

import { PerfectScrollbarModule, PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Ng5BreadcrumbModule, BreadcrumbService } from 'ng5-breadcrumb';
import { DeviceDetectorModule, DeviceDetectorService } from 'ngx-device-detector';
import { TourMatMenuModule } from 'ngx-tour-md-menu';
import { ToastrModule } from 'ngx-toastr';

import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireModule } from 'angularfire2'

import {
	MatSlideToggleModule, MatButtonModule, MatBadgeModule, MatCardModule, MatMenuModule, MatToolbarModule, MatIconModule, MatInputModule, MatDatepickerModule, MatNativeDateModule, MatProgressSpinnerModule,
	MatTableModule, MatExpansionModule, MatSelectModule, MatSnackBarModule, MatTooltipModule, MatChipsModule, MatListModule, MatSidenavModule,
	MatTabsModule, MatProgressBarModule, MatCheckboxModule, MatSliderModule, MatRadioModule, MatDialogModule, MatGridListModule, MatPaginatorModule, MatPaginatorIntl,
	MAT_DATE_LOCALE
} from '@angular/material';

import { RoutingModule } from "./app-routing.module";
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { LoadingBarModule } from '@ngx-loading-bar/core';

import { AuthService } from './service/auth-service/auth.service';
import { PageTitleService } from './core/page-title/page-title.service';
import { D3ChartService } from "./core/nvD3/nvD3.service";

import { GeneAppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { MenuToggleModule } from './core/menu/menu-toggle.module';
import { MenuItems } from './core/menu/menu-items/menu-items';
import { AuthGuard } from './core/guards/auth.guard';
import { HorizontalMenuItems } from './core/menu/horizontal-menu-items/horizontal-menu-items';

import { WidgetComponentModule } from './widget-component/widget-component.module';
import { HorizontalLayoutComponent } from './horizontal-layout/horizontal-layout.component';
import { PagesModule } from './pages/pages.module';
import { DocumentComponent } from './acreditacion/masters/document/document.component';
import { DialogDeleteComponent } from './acreditacion/dialog-delete/dialog-delete.component';
import { DialogComponent } from './acreditacion/dialog/dialog.component';
import { DocumentFormComponent } from './acreditacion/masters/document/document-form/document-form.component';
import { EvaluacionesComponent } from './acreditacion/pages/evaluaciones/evaluaciones.component';
import { UserProfileComponent } from './acreditacion/pages/user-profile/user-profile.component';
import { UserProfileFormComponent } from './acreditacion/pages/user-profile/user-profile-form/user-profile-form.component';
import { UserTypeComponent } from './acreditacion/configuration/user-type/user-type.component';
import { UserComponent } from './acreditacion/configuration/user/user.component';
import { FormUserComponent } from './acreditacion/configuration/user/form-user/form-user.component';
import { FormUserTypeComponent } from './acreditacion/configuration/user-type/form-user-type/form-user-type.component';
import { FormProcessComponent } from './acreditacion/pages/evaluaciones/form-process/form-process.component';
import { PhaseComponent } from './acreditacion/pages/phase/phase.component';
import { ProcessComponent } from './acreditacion/pages/process/process.component';
import { CustomPaginator } from './acreditacion/pages/evaluaciones/custom-paginator';
import { FormPhaseComponent } from './acreditacion/pages/process/form-phase/form-phase.component';
import { CriteriaComponent } from './acreditacion/pages/criteria/criteria.component';
import { FormCriteriaComponent } from './acreditacion/pages/criteria/form-criteria/form-criteria.component';
import { IndicatorComponent } from './acreditacion/pages/indicator/indicator.component';
import { FormIndicatorComponent } from './acreditacion/pages/indicator/form-indicator/form-indicator.component';
import { TaskComponent } from './acreditacion/pages/task/task.component';
import { FormTaskComponent } from './acreditacion/pages/task/form-task/form-task.component';
import { FormTaskManagementComponent } from './acreditacion/pages/task/form-task-management/form-task-management.component';
import { ControlComponent } from './acreditacion/pages/control/control.component';
import { ReportComponent } from './acreditacion/pages/report/report.component';
import { ListTaskComponent } from './acreditacion/pages/taskManagement/list-task/list-task.component';
import { TaskProccessComponent } from './acreditacion/pages/taskManagement/task-proccess/task-proccess.component';
import { MatStepperModule } from '@angular/material/stepper';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { ToolsComponent } from './acreditacion/pages/taskManagement/tools/tools.component';
import { GesUserPermsComponent } from './acreditacion/pages/evaluaciones/ges-user-perms/ges-user-perms.component'
import { RestartLoginComponent } from './session/loginV2/restart-login/restart-login.component'
import { FormCourseComponent } from './acreditacion/pages/tools/rubric/form-course/form-course.component';
import { GradeComponent } from './acreditacion/pages/tools/grade/grade.component';
import { RubricComponent } from './acreditacion/pages/tools/rubric/rubric.component';
import { FormCompetenceComponent } from './acreditacion/pages/course/form-competence/form-competence.component';
import { CourseComponent } from './acreditacion/pages/course/course.component';
export const firebaseConfig = {
	apiKey: "AIzaSyBO0CLP4fOA_kanqw1HQ2sDjEkyuK9lQ3o",
	authDomain: "gene-ccf5f.firebaseapp.comm",
	databaseURL: "https://gene-ccf5f.firebaseio.com",
	projectId: "gene-ccf5fc",
	storageBucket: "gene-ccf5f.appspot.com",
	messagingSenderId: "907778578362"
}

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
	return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
	suppressScrollX: true
};

@NgModule({
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		FormsModule,
		ReactiveFormsModule,
		DeviceDetectorModule.forRoot(),
		RoutingModule,
		FlexLayoutModule,
		PagesModule,
		NgbModalModule.forRoot(),
		Ng5BreadcrumbModule.forRoot(),
		TourMatMenuModule.forRoot(),
		PerfectScrollbarModule,
		MenuToggleModule,
		HttpClientModule,
		MatSlideToggleModule,
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient]
			}
		}),
		AngularFireModule.initializeApp(firebaseConfig),
		AngularFireAuthModule,
		MatButtonModule,
		MatCardModule,
		MatMenuModule,
		MatToolbarModule,
		MatIconModule,
		MatBadgeModule,
		MatInputModule,
		MatDatepickerModule,
		MatNativeDateModule,
		MatProgressSpinnerModule,
		MatTableModule,
		MatExpansionModule,
		MatSelectModule,
		MatSnackBarModule,
		MatTooltipModule,
		MatChipsModule,
		MatListModule,
		MatSidenavModule,
		MatTabsModule,
		MatProgressBarModule,
		MatCheckboxModule,
		MatSliderModule,
		MatRadioModule,
		MatDialogModule,
		MatGridListModule,
		MatPaginatorModule,
		ToastrModule.forRoot(),
		WidgetComponentModule,
		LoadingBarRouterModule,
		LoadingBarRouterModule,
		MatStepperModule,
		PdfViewerModule
	],
	declarations: [
		GeneAppComponent,
		MainComponent,
		UserProfileFormComponent,
		HorizontalLayoutComponent, 
		DocumentComponent, 
		DocumentFormComponent, 
		DialogDeleteComponent, 
		DialogComponent, 
		EvaluacionesComponent,
		UserTypeComponent, 
		UserComponent, 
		FormUserComponent, 
		FormUserTypeComponent, 
		FormProcessComponent, 
		PhaseComponent, 
		ProcessComponent, 
		FormPhaseComponent, 
		CriteriaComponent, 
		FormCriteriaComponent, 
		IndicatorComponent, 
		FormIndicatorComponent, 
		TaskComponent, 
		FormTaskComponent, 
		FormTaskManagementComponent, 
		ControlComponent, 
		ReportComponent, 
		ListTaskComponent, 
		TaskProccessComponent, 
		ToolsComponent,
		GradeComponent,
		RubricComponent,
		UserProfileComponent,
		GesUserPermsComponent,
		RestartLoginComponent,
		FormCourseComponent,
		FormCompetenceComponent,
		CourseComponent
	],
	bootstrap: [GeneAppComponent],
	entryComponents: [DialogDeleteComponent, DialogComponent, FormUserTypeComponent, FormUserComponent, FormProcessComponent, FormPhaseComponent, FormCriteriaComponent, FormIndicatorComponent, FormTaskComponent, FormTaskManagementComponent, ToolsComponent,
		HorizontalLayoutComponent, DocumentComponent, DocumentFormComponent, EvaluacionesComponent, UserTypeComponent, UserComponent, PhaseComponent, ProcessComponent, CriteriaComponent, IndicatorComponent, TaskComponent, FormTaskManagementComponent, ControlComponent, ReportComponent, ListTaskComponent, TaskProccessComponent, GradeComponent, RubricComponent,
		UserProfileComponent, UserProfileFormComponent,
		GesUserPermsComponent, RestartLoginComponent,FormCourseComponent,FormCompetenceComponent,CourseComponent
	],
	providers: [
		{ provide: MatPaginatorIntl, useClass: CustomPaginator },
		{ provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
		D3ChartService,
		MenuItems,
		HorizontalMenuItems,
		BreadcrumbService,
		PageTitleService,
		AuthService,
		{
			provide: PERFECT_SCROLLBAR_CONFIG,
			useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
		},
		AuthGuard
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GeneAppModule { }
