import { FormCourseComponent } from './form-course/form-course.component';
import { UserType } from '../../../models/usertype';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatDialog, MatSnackBar, MatTableDataSource, MatPaginatorIntl } from '@angular/material';
import { MasterService } from 'app/acreditacion/services/master.service';
import { DialogDeleteComponent } from 'app/acreditacion/dialog-delete/dialog-delete.component';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { Router } from '@angular/router';

@Component({
  selector: 'rubric',
  templateUrl: './rubric.component.html',
  styleUrls: ['./rubric.component.scss']
})
export class RubricComponent implements OnInit {


  @ViewChild(MatPaginatorIntl) paginator: MatPaginatorIntl;

  @ViewChild(MatSort) sort: MatSort

  isLoad: boolean = false

  data=[{course:"matematica",competence:"2",type:"avanzado",description:"curso de matematicas"}]

  displayedColumns: string[] = ['curso', 'competencia','tipo','descripcion', 'action'];
  
  dataSource: any


  constructor(private route: Router,private masterService: MasterService,
    private dialog: MatDialog, private snackBar: MatSnackBar,private pageTitleService: PageTitleService
  ) { }

  ngOnInit() {
    this.loadCourses();
    this.pageTitleService.setTitle("Rúbricas");
  }
  applyFilter(filterValue: String) {
    this.dataSource.filter = filterValue.trim().toLowerCase()
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage()
    }
  }
  private loadCourses() {

      this.dataSource = new MatTableDataSource(this.data)
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.isLoad = false
    
  }
  openCourse(data) {
    this.route.navigate(['course/'+data.id,{name:data.course}])
  }

  onSetUserType(data: UserType) {
    this.isLoad = true
    this.masterService.addUserType(data).subscribe(() => {
      this.loadCourses()
      this.isLoad = false
      this.openSnackBar("Se añadio correctamente", "ÉXITO")
    }, error => {
      this.isLoad = false
      this.openSnackBar(error.name, 'Error')

    })
  }
  onEditUserType(data: any) {
    this.isLoad = true

    this.masterService.updateUserType(data).subscribe(() => {
      this.loadCourses()
      this.openSnackBar("Se editó corectamente", "ÉXITO")

    }, error => {
      this.isLoad = false
      this.openSnackBar(error.name, 'Error')

    })
  }
  onUpdateStatus(data: UserType) {

    let status: string = (~~(!parseInt(data.status))).toString()
    let UserType1: UserType = new UserType(data.id,data.name, status)
    this.onEditUserType(UserType1)
  }
  onDeleteUserType(id: any) {
    this.isLoad = true
    this.masterService.deleteUserType(id).subscribe(() => {
      this.loadCourses()
      this.isLoad = false
      this.openSnackBar("Se eliminó correctamente", "ÉXITO")
    }, error => {
      this.openSnackBar(error.name, 'Error')
      this.isLoad = false

    })
  }

  openModal(data: any) {
    let rpv1: UserType
    if (data) {
      rpv1 = new UserType(data.id,data.name,data.status)
    } else {
      rpv1 = null
    }

    const dialogRef = this.dialog.open(FormCourseComponent, {
      data: rpv1,
      width: '500px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result == 'Close') {
        console.log('close')
      } else {
        if (result.id == null) {
          this.onSetUserType(result)
        } else {
          this.onEditUserType(result)
        }
      }
    })
  }
  // pregunta de eliminar
  onDelete(i: any) {
    const dialogRef = this.dialog.open(DialogDeleteComponent,{data:"¿Estás seguro de que quieres eliminar este Tipo de Usuario de forma permanente?", width:'450px'})

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        this.onDeleteUserType(i.id)
      }
    })
  } 

  //snackBar
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      verticalPosition: 'top'
    });
  }
}