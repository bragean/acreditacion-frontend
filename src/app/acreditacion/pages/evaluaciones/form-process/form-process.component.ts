//import { AuthService } from 'app/service/auth-service/auth.service';
import { AuthService } from 'app/acreditacion/services/auth.service';
import { MasterService } from './../../../services/master.service';
import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ErrorService } from 'app/acreditacion/services/error.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ms-form-process',
  templateUrl: './form-process.component.html',
  styleUrls: ['./form-process.component.scss']
})
export class FormProcessComponent implements OnInit {

  public form: FormGroup
  public id: any = ''
  public users: any[] = []
  public user: any

  searchUser = ''

  constructor(private fb: FormBuilder, 
    public dialogRef: MatDialogRef<FormProcessComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private masterService: MasterService,
    private error:ErrorService,
    private toastr: ToastrService, 
    private auth: AuthService) {
    this.user = this.auth.getLSuser()
    this.form = this.fb.group({
      id: null,
      name: null,
      user_list: [[]],
      documents_list: '',
      description: null,
      status: true
    })
  }

  ngOnInit() {
    this.initForm()
  }

  getUsers() {
    this.masterService.getUsers().subscribe((data: any) => {
      this.users = data
    },
    error=>{
      this.error.getError(error)
    })
  }
  getId() {
    return this.user.toString()
  }

  onCancel(): void {
    this.dialogRef.close('Cancel')
  }

  initForm() {
    if (this.data == null) {
      this.id = null

    }
    else {
      this.id = this.data.id
      this.form.setValue({
        id: this.data.id,
        name: this.data.name,
        user_list: this.data.user_list == '' ? [] : this.data.user_list.split(','),
        documents_list: '',
        description: this.data.description,
        status: this.data.status
      })
    }
  }

  onSubmit(data: any) {
    if (this.form.valid) {
      this.dialogRef.close({
        id: this.form.value.id,
        name: this.form.value.name,
        user_list: this.form.value.user_list.toString(),
        documents_list: this.form.value.documents_list,
        description: this.form.value.description,
        status: this.form.value.status
      })
      this.form.reset()
    }
  }
  onClose() {
    this.dialogRef.close('Close')
    this.form.reset()
  }

  deleteUsername(item) {
    let aux: [] = []
    aux = this.form.get("user_list").value

    aux.splice(item, 1)
    this.form.get("user_list").setValue(aux)
  }


  onsearchUser() {

    if (this.form.get('user_list').value.includes(this.searchUser)) {
      this.toastr.error('Ya existe el usuario','Error')
      return
    }
    if(this.user.username==this.searchUser){
      this.toastr.error('Usted ya pertenece al proceso','Error')
      return
    }
    let us = {
      username: this.searchUser
    }
    this.masterService.searchUser(us).subscribe((res: any) => {
      if (res.response == "succes") {
        let r: any[] = this.form.get('user_list').value
        r.push(us.username)
        this.form.get("user_list").setValue(r)
      } else {
        this.toastr.error('Usuario no encontrado','Error')
      }
    },
    error=>{
      this.error.getError(error)
    })
  }

}