import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTaskManagementComponent } from './form-task-management.component';

describe('FormTaskManagementComponent', () => {
  let component: FormTaskManagementComponent;
  let fixture: ComponentFixture<FormTaskManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormTaskManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTaskManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
