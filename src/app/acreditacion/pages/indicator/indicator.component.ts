import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { DialogDeleteComponent } from 'app/acreditacion/dialog-delete/dialog-delete.component';
import { MatTableDataSource, MatSnackBar, MatDialog, MatPaginatorIntl, MatSort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterService } from 'app/acreditacion/services/master.service';
import { FormIndicatorComponent } from './form-indicator/form-indicator.component';
import { AuthService } from 'app/acreditacion/services/auth.service';
import { ErrorService } from 'app/acreditacion/services/error.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ms-indicator',
  templateUrl: './indicator.component.html',
  styleUrls: ['./indicator.component.scss']
})
export class IndicatorComponent implements OnInit {
  @Input() processPadre: string

  @ViewChild(MatPaginatorIntl) paginator: MatPaginatorIntl;

  @ViewChild(MatSort) sort: MatSort

  isLoad: boolean = false


  displayedColumns: string[] = ['name', 'description', 'action'];
  dataSource: any

  id_process = ''
  id_Criteria=''

  listCriterias:any[]=[]

  public idProcess:any;

  constructor(private masterService: MasterService,
    private dialog: MatDialog, private snackBar: MatSnackBar,
    private route: Router, private ar: ActivatedRoute,
    private authService: AuthService,
    private error: ErrorService,
    private toastr: ToastrService) 
    { }

  ngOnInit() {   
    this.configurePage();
  }

  public configurePage(){
    if(this.masterService.process!=null){
      this.id_process=this.masterService.process.id;
      this.loadCriterias();
    }
  }

  openIndicator(data) {
    this.route.navigate(['indicator/' + data.id])
  }

  applyFilter(filterValue: String) {
    this.dataSource.filter = filterValue.trim().toLowerCase()
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage()
    }
  }

  public loadIndicatorsByCriteria(id){
    this.loadIndicators()
  }

  private loadCriterias(){
    this.masterService.getCriteriasByProcess(this.id_process).subscribe((data: any) => {
      this.listCriterias=data
    },
      error => {      
        this.error.getError(error)
      })
  }

  private loadIndicators() {

    this.isLoad = true
    this.masterService.getIndicatorsByProcess(this.id_process).subscribe((data: any) => {

      var listIndicators:any[]=[]
      for(var i=0;i<data.length;i++){
        if(this.id_Criteria==data[i].id_criteria){
          listIndicators.push(data[i])
        }
      }

      this.dataSource = new MatTableDataSource(listIndicators)
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.isLoad = false
    },
      error => {
        this.error.getError(error)
        this.isLoad = false
      })
  }

  onSetIndicator(data: any) {
    this.isLoad = true
    this.masterService.addIndicator(data).subscribe(() => {
      this.loadIndicators()
      this.isLoad = false
      this.toastr.success('Se añadió correctamente', 'ÉXITO')
    }, error => {
      this.error.getError(error)
      this.isLoad = false
    })
  }

  onEditIndicator(data: any) {
    this.isLoad = true

    let send = {
      name: data.name,
      description: data.description,
      status: data.status
    }

    this.masterService.updateIndicator(send, data.id).subscribe(() => {
      this.loadIndicators()
      this.toastr.success('Se editó correctamente', 'ÉXITO')

    }, error => {
      this.error.getError(error)
      this.isLoad = false
    })
  }

  onUpdateStatus(data: any) {

    /* let status: string = (~~(!parseInt(data.status))).toString()
    let Indicator1: Indicator = new Indicator(data.id,data.name, status)
    this.onEditIndicator(Indicator1) */
  }

  onDeleteIndicator(id: any) {
    this.isLoad = true
    this.masterService.deleteIndicator(id).subscribe(() => {
      this.loadIndicators()
      this.isLoad = false
      this.toastr.success('Se eliminó correctamente', 'ÉXITO')
    }, error => {
      this.error.getError(error)
      this.isLoad = false
    })
  }

  openModal(data: any) {
    const dialogRef = this.dialog.open(FormIndicatorComponent, {
      data: data,
      width: '500px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result == 'Close') {
      } else {
        if (result.id == null) {
          this.onSetIndicator(result)
        } else {
          this.onEditIndicator(result)
        }
      }
    })
  }

  // pregunta de eliminar
  onDelete(i: any) {
    const dialogRef = this.dialog.open(DialogDeleteComponent, { data: "¿Estás seguro de que quieres eliminar este indicador de forma permanente? se eliminaran las tareas asociadas", width: '450px' })

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        this.onDeleteIndicator(i.id)
      }
    })
  }

  //snackBar
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      verticalPosition: 'top'
    });
  }

  public checkRole(){
    return this.masterService.rol;
  }

  public reload(){
    this.loadCriterias();
  }
}