import { AuthService } from 'app/acreditacion/services/auth.service';
import { Router } from '@angular/router';
import { ErrorService } from 'app/acreditacion/services/error.service';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatDialog, MatSnackBar, MatTableDataSource } from '@angular/material';
import { MasterService } from 'app/acreditacion/services/master.service';
import { DialogDeleteComponent } from 'app/acreditacion/dialog-delete/dialog-delete.component';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from 'app/acreditacion/models/user';
import { UserProfileFormComponent } from 'app/acreditacion/pages/user-profile/user-profile-form/user-profile-form.component'

@Component({
  selector: 'ms-userprofile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
  host: {
    "[@fadeInAnimation]": 'true'
  },
  //animations: [ fadeInAnimation ]
})
export class UserProfileComponent implements OnInit {

  fields: string[] = ['id', 'first_name', 'last_name', 'username', 'user_type', 'status'];
  user: any = '';
  id_user: any = '';
  userType: any = '';
  status: any = '';

  constructor(
    private pageTitleService: PageTitleService,
    private masterService: MasterService,
    private auth: AuthService,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private error: ErrorService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.pageTitleService.setTitle("Perfil de Usuario");
    this.getUser()
  }
  private loadUser() {
    this.masterService.getProfile().subscribe((data: any) => {
      console.log("profile data")
      console.log(data);
      this.user = data
    },
      error => {
        this.error.getError(error)
        console.log('MSAP: Error for User profile')
      })
  }
  getUser() {
    this.user = this.authService.getLSuser()
    console.log(this.user)
    this.id_user = this.user.id
    console.log("el id")
    console.log(this.id_user)
    this.status = this.user.status;
    this.loadUser()
  }

  onEditUser(data: any) {

    let formData = new FormData()
    formData.append('photo', data.photo)
    formData.append('first_name', data.first_name)
    formData.append('last_name', data.last_name)

    this.masterService.updateUser(data).subscribe(() => {
      this.loadUser()
      this.openSnackBar("Se editó corectamente", "ÉXITO")

    }, error => {
      this.openSnackBar(error.name, 'Error')
    })
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      verticalPosition: 'top'
    });
  }

  openModal() {


    const dialogRef = this.dialog.open(UserProfileFormComponent, {
      data: this.user,
      width: '500px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result == 'Close') {
        console.log('close')
      } else {
        this.onEditUser(result)
      }
    })
  }
}
